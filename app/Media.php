<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $guarded = [];

    public function mdUsers()
    {
        return $this->belongsTo(User::class, 'md_id', 'id');
    }

    public function adUsers()
    {
        return $this->belongsTo(Advertising::class, 'ad_id', 'ad_id');
    }
}
