<?php

namespace App\Http\Controllers\media;

use App\ApiAccounts;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\Contracts\ReportRepository;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;

class mdreportController extends Controller
{
    protected $mdUser;
    protected $report;
    protected $media;

    public function __construct(MdUserRepository $mdUser,
                                MediaRepository $media,
                                ReportRepository $report)
    {
        $this->mdUser = $mdUser;
        $this->media = $media;
        $this->getMdUsers();
        $this->report = $report;

    }

    public function Monthly(Request $request)
    {
        $mdUserss_id = $request->session()->get('mdUserss_id');
        $media_list = $this->media
            ->withCriteria([
                new EagerLoad(['mdUsers', 'adUsers'])
            ])
            ->findwhere('md_id', $mdUserss_id);

        if (!isset($request->sch_media)) {
            $sch_media = "";
        } else {
            $sch_media = $request->sch_media;
        }

        if (!isset($request->sch)) {
            $sch = date("Y");
        } else {
            $sch = $request->sch;
        }

        $sch1 = date("{$sch}-01-01");
        $sch2 = date("{$sch}-12-30");

        $results = ApiAccounts::where('media_id', '=', $mdUserss_id)
            ->where('created_at', '>=', $sch1." 00:00:00")
            ->where('created_at', '<=', $sch2." 00:00:00")
            ->when($request->sch_media,
                function ($q) use ($request, $sch_media) {
                    return $q->where('media_name', '=', $sch_media);
                }
            )
            ->orderby('created_at', 'asc')
            ->get();

        $date_range = $this->date_range($sch1, $sch2, "+1 month", "Y-m");

        $taproad_count = 0;
        $tapjoy_count = 0;
        $appall_count = 0;

        $data = array_map(function ($item) use ($results, $taproad_count, $tapjoy_count, $appall_count) {
            $taproad_point = $results->sum(function ($results_item) use ($item, &$taproad_count) {
                if ($item == date("Y-m", strtotime($results_item->created_at)) && $results_item->advertising_name == "taproad") {
                    if (is_numeric($results_item->user_point)) {
                        $taproad_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $tapjoy_point = $results->sum(function ($results_item) use ($item, &$tapjoy_count) {
                if ($item == date("Y-m", strtotime($results_item->created_at)) && $results_item->advertising_name == "tapjoy") {
                    if (is_numeric($results_item->user_point)) {
                        $tapjoy_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $appall_point = $results->sum(function ($results_item) use ($item, &$appall_count) {
                if ($item == date("Y-m", strtotime($results_item->created_at)) && $results_item->advertising_name == "appall") {
                    if (is_numeric($results_item->user_point)) {
                        $appall_count++;
                        return $results_item->user_point;
                    }
                }
            });

            return ['date'=>$item,
                    'taproad_point'=>$taproad_point,
                    'tapjoy_point'=>$tapjoy_point,
                    'appall_point'=>$appall_point,
                    'taproad_count' => $taproad_count,
                    'tapjoy_count' => $tapjoy_count,
                    'appall_count' => $appall_count];
        }, $date_range);

        $total['taproad_point'] = 0;
        $total['tapjoy_point'] = 0;

        $total['taproad_count'] = 0;
        $total['tapjoy_count'] = 0;

        $total['appall_point'] = 0;
        $total['appall_count'] = 0;

        foreach ($data as $key => $value) {
            $datas[] = [
                "date" => $value['date'],
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "appall_point" => $value['appall_point'],
                "taproad_count" => $value['taproad_count'],
                "tapjoy_count" => $value['tapjoy_count'],
                "appall_count" => $value['appall_count'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $chat_datas[$value['date']] = [
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "appall_point" => $value['appall_point'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $total['taproad_point'] = (int) $total['taproad_point'] + (int) $value['taproad_point'];
            $total['tapjoy_point'] = (int) $total['tapjoy_point'] + (int) $value['tapjoy_point'];
            $total['appall_point'] = (int) $total['appall_point'] + (int) $value['appall_point'];
            $total['taproad_count'] = (int) $total['taproad_count'] + (int) $value['taproad_count'];
            $total['tapjoy_count'] = (int) $total['tapjoy_count'] + (int) $value['tapjoy_count'];
            $total['appall_count'] = (int) $total['appall_count'] + (int) $value['appall_count'];
        }

        $chart_width = 100;

        if (count($chat_datas) > 8) {
            $chart_width = count($chat_datas) * 16;
        }

        return view('media.report.monthly', compact('datas', 'sch', 'chat_datas',  'chart_width', 'total','media_list', 'sch_media'));
    }

    public function Daily(Request $request)
    {
        $mdUserss_id = $request->session()->get('mdUserss_id');
        $media_list = $this->media
            ->withCriteria([
                new EagerLoad(['mdUsers', 'adUsers'])
            ])
            ->findwhere('md_id', $mdUserss_id);

        if (!isset($request->sch_media)) {
            $sch_media = "";
        } else {
            $sch_media = $request->sch_media;
        }

        if (isset($request->sch)) {
            $date = explode(" ~ ", $request->sch);
            $start_date = $date[0];
            $end_date = $date[1];
        } else {
            $start_date = date("Y")."-".date("m")."-"."01";
            $end_date  = date("Y")."-".date("m")."-".date("t", mktime(0, 0, 1, date("m"), 1, date("Y")));
        }

        $results = ApiAccounts::where('media_id', '=', $mdUserss_id)
            ->where('created_at', '>=', $start_date." 00:00:00")
            ->where('created_at', '<=', $end_date." 00:00:00")
            ->when($request->sch_media,
                function ($q) use ($request, $sch_media) {
                    return $q->where('media_name', '=', $sch_media);
                }
            )
            ->orderby('created_at', 'asc')
            ->get();

        $date_range = $this->date_range($start_date, $end_date, "+1 day", "Y-m-d");

        $taproad_count = 0;
        $tapjoy_count = 0;
        $appall_count = 0;

        $data = array_map(function ($item) use ($results, $taproad_count, $tapjoy_count ,$appall_count) {
            $taproad_point = $results->sum(function ($results_item) use ($item, &$taproad_count) {
                if ($item == date("Y-m-d", strtotime($results_item->created_at)) && $results_item->advertising_name == "taproad") {
                    if (is_numeric($results_item->user_point)) {
                        $taproad_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $tapjoy_point = $results->sum(function ($results_item) use ($item, &$tapjoy_count) {
                if ($item == date("Y-m-d", strtotime($results_item->created_at)) && $results_item->advertising_name == "tapjoy") {
                    if (is_numeric($results_item->user_point)) {
                        $tapjoy_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $appall_point = $results->sum(function ($results_item) use ($item, &$appall_count) {
                if ($item == date("Y-m-d", strtotime($results_item->created_at)) && $results_item->advertising_name == "appall") {
                    if (is_numeric($results_item->user_point)) {
                        $appall_count++;
                        return $results_item->user_point;
                    }
                }
            });

            return ['date'=>$item,
                'taproad_point'=>$taproad_point,
                'tapjoy_point'=>$tapjoy_point,
                'appall_point'=>$appall_point,
                'taproad_count' => $taproad_count,
                'tapjoy_count' => $tapjoy_count,
                'appall_count' => $appall_count];
        }, $date_range);

        $total['taproad_point'] = 0;
        $total['tapjoy_point'] = 0;

        $total['taproad_count'] = 0;
        $total['tapjoy_count'] = 0;

        $total['appall_point'] = 0;
        $total['appall_count'] = 0;

        foreach ($data as $key => $value) {
            $datas[] = [
                "date" => $value['date'],
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "appall_point" => $value['appall_point'],
                "taproad_count" => $value['taproad_count'],
                "tapjoy_count" => $value['tapjoy_count'],
                "appall_count" => $value['appall_count'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $chat_datas[$value['date']] = [
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "appall_point" => $value['appall_point'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $total['taproad_point'] = (int) $total['taproad_point'] + (int) $value['taproad_point'];
            $total['tapjoy_point'] = (int) $total['tapjoy_point'] + (int) $value['tapjoy_point'];
            $total['appall_point'] = (int) $total['appall_point'] + (int) $value['appall_point'];
            $total['taproad_count'] = (int) $total['taproad_count'] + (int) $value['taproad_count'];
            $total['tapjoy_count'] = (int) $total['tapjoy_count'] + (int) $value['tapjoy_count'];
            $total['appall_count'] = (int) $total['appall_count'] + (int) $value['appall_count'];
        }

        $table_date = $start_date;

        $chart_width = 100;

        if (count($chat_datas) > 8) {
            $chart_width = count($chat_datas) * 16;
        }

        return view('media.report.donthly', compact('datas','chat_datas','start_date', 'end_date', 'table_date','chart_width', 'total','media_list', 'sch_media'));
    }

    public function Hourly(Request $request)
    {
        $mdUserss_id = $request->session()->get('mdUserss_id');
        $media_list = $this->media
            ->withCriteria([
                new EagerLoad(['mdUsers', 'adUsers'])
            ])
            ->findwhere('md_id', $mdUserss_id);

        if (!isset($request->sch_media)) {
            $sch_media = "";
        } else {
            $sch_media = $request->sch_media;
        }

        if (!isset($request->sch)) {
            $sch = date("Y-m-d");
        } else {
            $sch = $request->sch;
        }

        $sch1 = date("{$sch} 00:00:00");
        $sch2 = date("{$sch} 23:59:59");

        $results = ApiAccounts::where('media_id', '=', $mdUserss_id)
            ->where('created_at', '>=', $sch1." 00:00:00")
            ->where('created_at', '<=', $sch2." 00:00:00")
            ->when($request->sch_media,
                function ($q) use ($request, $sch_media) {
                    return $q->where('media_name', '=', $sch_media);
                }
            )
            ->orderby('created_at', 'asc')
            ->get();

        $date_range = $this->date_range($sch1, $sch2, "+1 hours", "Y-m-d H");

        $taproad_count = 0;
        $tapjoy_count = 0;
        $appall_count = 0;

        $data = array_map(function ($item) use ($results, $taproad_count, $tapjoy_count, $appall_count) {
            $taproad_point = $results->sum(function ($results_item) use ($item, &$taproad_count) {
                if ($item == date("Y-m-d H", strtotime($results_item->created_at)) && $results_item->advertising_name == "taproad") {
                    if (is_numeric($results_item->user_point)) {
                        $taproad_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $tapjoy_point = $results->sum(function ($results_item) use ($item, &$tapjoy_count) {
                if ($item == date("Y-m-d H", strtotime($results_item->created_at)) && $results_item->advertising_name == "tapjoy") {
                    if (is_numeric($results_item->user_point)) {
                        $tapjoy_count++;
                        return $results_item->user_point;
                    }
                }
            });

            $appall_point = $results->sum(function ($results_item) use ($item, &$appall_count) {
                if ($item == date("Y-m-d H", strtotime($results_item->created_at)) && $results_item->advertising_name == "appall") {
                    if (is_numeric($results_item->user_point)) {
                        $appall_count++;
                        return $results_item->user_point;
                    }
                }
            });

            return ['date'=>$item,
                'taproad_point'=>$taproad_point,
                'tapjoy_point'=>$tapjoy_point,
                'appall_point'=>$appall_point,
                'taproad_count' => $taproad_count,
                'tapjoy_count' => $tapjoy_count,
                'appall_count' => $appall_count];
        }, $date_range);

        $total['taproad_point'] = 0;
        $total['tapjoy_point'] = 0;

        $total['taproad_count'] = 0;
        $total['tapjoy_count'] = 0;

        $total['appall_point'] = 0;
        $total['appall_count'] = 0;

        foreach ($data as $key => $value) {
            $datas[] = [
                "date" => $value['date'],
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "appall_point" => $value['appall_point'],
                "taproad_count" => $value['taproad_count'],
                "tapjoy_count" => $value['tapjoy_count'],
                "appall_count" => $value['appall_count'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $chat_datas[$value['date'] . "시"] = [
                "taproad_point" => $value['taproad_point'],
                "appall_point" => $value['appall_point'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']+$value['appall_point']
            ];

            $total['taproad_point'] = (int) $total['taproad_point'] + (int) $value['taproad_point'];
            $total['tapjoy_point'] = (int) $total['tapjoy_point'] + (int) $value['tapjoy_point'];
            $total['appall_point'] = (int) $total['appall_point'] + (int) $value['appall_point'];
            $total['taproad_count'] = (int) $total['taproad_count'] + (int) $value['taproad_count'];
            $total['tapjoy_count'] = (int) $total['tapjoy_count'] + (int) $value['tapjoy_count'];
            $total['appall_count'] = (int) $total['appall_count'] + (int) $value['appall_count'];
        }

        $chart_width = 100;

        if (count($chat_datas) > 8) {
            $chart_width = count($chat_datas) * 16;
        }

        return view('media.report.hourly', compact('datas', 'sch', 'chat_datas',  'chart_width', 'total','media_list', 'sch_media'));
    }

    public function ByAdvertiser()
    {
        return view('media.report.byadvertiser');
    }

    public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' )
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while( $current <= $last ) {
            //$dates[] = date($output_format, $current);
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }
}
