<?php

namespace App\Http\Controllers\media;

use App\ApiAccounts;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;

class mdlogController extends Controller
{
    protected $mdUser;

    public function __construct(MdUserRepository $mdUser, MediaRepository $media)
    {
        $this->mdUser = $mdUser;
        $this->media = $media;
        $this->getMdUsers();
    }

    public function index(Request $request)
    {
        $mdUserss_id = $request->session()->get('mdUserss_id');
        $media_list = $this->media
            ->withCriteria([
                new EagerLoad(['mdUsers', 'adUsers'])
            ])
            ->findwhere('md_id', $mdUserss_id);

        if (!isset($request->sch_media)) {
            $sch_media = "";
        } else {
            $sch_media = $request->sch_media;
        }

        if (isset($request->sch)) {
            $date = explode(" ~ ", $request->sch);
            $start_date = $date[0];
            $end_date = $date[1];
        } else {
//            $start_date = date("Y")."-".date("m")."-"."01";
//            $end_date  = date("Y")."-".date("m")."-".date("t", mktime(0, 0, 1, date("m"), 1, date("Y")));
            $start_date = date("Y-m-d");
            $end_date  = date("Y-m-d");
        }

        $account = ApiAccounts::where('user_point', '!=', '')
            ->where('created_at', '>=', $start_date." 00:00:00")
            ->where('created_at', '<=', $end_date." 23:59:59")
            ->when($request->user_id,
                function ($q) use ($request) {
                    return $q->where('user_id', 'LIKE', '%'.$request->user_id.'%');
                }
            )
            ->when($request->sch_media,
                function ($q) use ($request, $sch_media) {
                    return $q->where('media_name', '=', $sch_media);
                }
            )
            ->orderby('created_at', 'desc')
            ->get();


        return view('media.log.index', compact('account','start_date', 'end_date','media_list', 'sch_media'));
    }

}
