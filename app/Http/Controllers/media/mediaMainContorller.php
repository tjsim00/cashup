<?php

namespace App\Http\Controllers\media;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;

class mediaMainContorller extends Controller
{
    protected $mdUser;
    protected $media;

    public function __construct(MdUserRepository $mdUser, MediaRepository $media)
    {
        $this->mdUser = $mdUser;
        $this->media = $media;

        $this->getMdUsers();
    }

    public function index()
    {
        $media_list = $this->media->withCriteria([
            new EagerLoad(['mdUsers','adUsers'])
        ])->Paginate(15);

        return view('media.main', compact( 'media_list'));
    }

    public function mdUserChange(Request $request)
    {
        $request_data = json_decode($request->data);

        if ($request_data->mdUserid) {
            $users = $this->mdUser->find($request_data->mdUserid);

            if ($users) {
                $request->session()->put('mdUserss_id', $users->id);
                $request->session()->put('mdUserss_user_id', $users->user_id);
            } else {
                $request->session()->forget('mdUserss_id');
                $request->session()->forget('mdUserss_user_id');
            }
        } else {
            $request->session()->forget('mdUserss_id');
            $request->session()->forget('mdUserss_user_id');
        }

        $result['result'] = "success";
        $response = response()->json($result, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);

        return $response;
    }
}
