<?php

namespace App\Http\Controllers\media;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\{AdUserRepository, MdUserRepository, MediaPreferencesRepository, MediaRepository};
use App\Repositories\Eloquent\Criteria\ByMdUid;
use App\Repositories\Eloquent\Criteria\ByMediaId;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;

class mdController extends Controller
{
    protected $mdUser;
    protected $media;
    protected $adUser;
    protected $mediaPreferences;

    public function __construct(MdUserRepository $mdUser, MediaRepository $media, AdUserRepository $adUser, MediaPreferencesRepository $mediaPreferences)
    {
        $this->mdUser = $mdUser;
        $this->media = $media;
        $this->adUser = $adUser;
        $this->mediaPreferences = $mediaPreferences;

        $this->getAdUsers();
        $this->getMdUsers();
    }

    public function index(Request $request)
    {
        $md_id = $request->session()->get('mdUserss_id');
        $media_list = $this->media->withCriteria([
            new EagerLoad(['mdUsers','adUsers'])
        ])->findWherePaginate('md_id', $md_id, 15);


        return view('media.md.index', compact( 'media_list'));
    }


    public function create()
    {
        return view('media.md.create');
    }


    public function store(Request $request)
    {
        $create_result = $this->media->create([
            'md_id' => $request->session()->get('mdUserss_id'),
            'ad_id' => $request['ad_id'],
            'name' => $request['name'],
            'site_url' => $request['site_url'],
            'biyul_ad' => $request['biyul_ad'],
            'biyul_md' => $request['biyul_md'],
            'exposure' => $request['exposure'],
            'adrestriction_lock_screen' => $request['adrestriction_lock_screen'],
            'adrestriction_noti' => $request['adrestriction_noti'],
            'bigo' => $request['bigo'],
        ]);

        $this->mediaPreferences->create([
            'md_uid' => $request->session()->get('mdUserss_id'),
            'media_id' => $create_result->id,
            'status' => 'off',
            'updatetime' => 0,
            'freepoint' => 0,
            'freelimit' => 0,
            'freequency' => 0,
        ]);


        return redirect()->route('mdsetup.index')->withSuccess(['Success Message here!']);
    }

    public function edit($id)
    {
        $media_list = $this->media->find($id);

        return view('media.md.edit', compact('media_list'));
    }


    public function update(Request $request, $id)
    {
        $this->media->update($id, [
            'md_id' => $request->session()->get('mdUserss_id'),
            'ad_id' => $request['ad_id'],
            'name' => $request['name'],
            'site_url' => $request['site_url'],
            'biyul_ad' => $request['biyul_ad'],
            'biyul_md' => $request['biyul_md'],
            'exposure' => $request['exposure'],
            'adrestriction_lock_screen' => $request['adrestriction_lock_screen'],
            'adrestriction_noti' => $request['adrestriction_noti'],
            'bigo' => $request['bigo'],
        ]);

        return redirect()->route('mdsetup.index')->withSuccess(['Success Message here!']);
    }


    public function destroy($id)
    {
        $this->media->delete($id);

        return redirect()->route('mdsetup.index')->withSuccess(['Success Message here!']);
    }

    public function preferences(Request $request, $id)
    {

        $mediaPreferences_list = $this->mediaPreferences->withCriteria([
            new ByMdUid($request->session()->get('mdUserss_id')),
            new ByMediaId($id)
        ])->all();

        return view('media.md.preferences_edit', compact('mediaPreferences_list'));
    }

    public function preferences_update(Request $request, $id)
    {

        $this->mediaPreferences->update($id, [
            'status' => $request['status'],
            'updatetime' => $request['updatetime'],
            'freepoint' => $request['freepoint'],
            'freelimit' => $request['freelimit'],
            'freequency' => $request['freequency'],
        ]);

        return redirect()->route('mdsetup.index')->withSuccess(['Success Message here!']);
    }
}
