<?php


namespace App\Http\Controllers;

use App\Advertising;
use App\ApiAccountsDetail;
use App\ApiAccounts;
use App\ApiBoundLog;
use App\ApiKdiskCallbacks;
use App\Media;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Support\Facades\View;
use Ixudra\Curl\Facades\Curl;

trait traitController
{
    /**
     * adUser 정보 갖고오기
     */
    public function getAdUsers()
    {
        $adUser_lists = $this->adUser->withCriteria([
            new ByTypeUser('ad'),
        ])->all();

        view::share(compact('adUser_lists'));
    }

    /**
     * mdUser 정보 갖고오기
     */
    public function getMdUsers()
    {
        $mdUser_lists = $this->mdUser->withCriteria([
            new ByTypeUser('md'),
        ])->all();

        view::share(compact('mdUser_lists'));
    }

    /**
     * @param $session_id
     * @param $kind
     * @param $from
     * @param $to
     * @param $type
     * @param $paramter
     * @param $method
     * @param $status
     * api 통신에서 response, request 기록 DB저장
     */
    public function apiLogSave($session_id, $kind, $from, $to, $type, $paramter, $method, $status)
    {
        ApiBoundLog::create([
            'session_id' => $session_id,
            'kind' => $kind,
            'from' => $from,
            'to' => $to,
            'type' => $type,
            'paramter' => $paramter,
            'method' => $method,
            'status' => $status,
        ]);
    }

    /**
     * @param $request
     * @param $info
     * @return mixed
     * 포인내역 저장 ApiAccounts에 간략 정보 저장
     * ApiAccountsDetail 상세 정보 저장
     */
    public function saveAccount($request, $info)
    {
        $accounts['session_id'] =  $info['session_id'];
        $accounts['media_id'] = media::where('name', $info['media_id'])->pluck('md_id')->first();
        $accounts['media_name'] = $info['media_id'];
        $accounts['advertising_id'] = advertising::where('title', $info['advertising_id'])->pluck('ad_id')->first();
        $accounts['advertising_name'] = $info['advertising_id'];
        $accounts['user_id'] =  $info['id'] ?? "";
        $accounts['user_point'] = $info['reward_quantity'] ?? "";
        $accounts['cam_id'] = $info['ad_idx'] ?? "";
        $accounts['cam_name'] = $info['reward_name'] ?? "";
        $accounts['status'] = $info['status'];

        $apiaccounts = ApiAccounts::create($accounts);


        $request['account_id'] = $apiaccounts->id;
        $request['session_id'] = $info['session_id'];
        $request['AppKey'] = $info['AppKey'] ?? "";
        $request['message'] = $info['message'];
        unset($request['id']);
        unset($request['Message']);


        ApiAccountsDetail::create($request->toArray());

        return $apiaccounts->id;
    }

    /**
     * @param $request
     * @param $info
     * @throws \Exception
     * kdisk에 포인트 전달 내역 저장
     */
    public function kdiskSend($request, $info)
    {
        $request['url'] = "https://m.kdisk.co.kr/cpc/charge/free_cashup_res.php";
        $param['session_id'] = session()->getId();
        $param['cam_id'] = $request['ad_idx'];
        $param['inv_id'] = "kdisk";
        $param['inv_hash'] = $info['id'];
        $param['uniqcode'] = IdGenerator::generate(['table' => 'api_kdisk_callbacks', 'field'=>'uniqcode','length' => 20, 'prefix' => date('Ym')."_"]);
        $param['cam_nm'] = urlencode($request['reward_name']);
        $param['pointval'] = $request['reward_quantity'];

        $output = implode('&', array_map(
            function ($v, $k) {
                if(is_array($v)){
                    return $k.'[]='.implode('&'.$k.'[]=', $v);
                }else{
                    return $k.'='.$v;
                }
            },
            $param,
            array_keys($param)
        ));

        $url = $request['url']."?".$output;

        $json_data = Curl::to($url)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->post();

        $param['status'] = $json_data->status;

        ApiKdiskCallbacks::create([
            'session_id'          =>  $param['session_id'],
            'cam_id'              =>  $param['cam_id'],
            'inv_id'              =>  $param['inv_id'],
            'inv_hash'            =>  $param['inv_hash'],
            'uniqcode'            =>  $param['uniqcode'],
            'cam_nm'              =>  $param['cam_nm'],
            'pointval'            =>  $param['pointval'],
            'url'               =>  $url,
            'status'            =>  $param['status']
        ]);
    }

    /**
     * @param $request
     * @return bool
     * 예외처리 아이디
     */
    public function exception($request)
    {
        $from_id = ['test01'];

        if (in_array($request['user_id'], $from_id) || in_array($request['id'], $from_id) || in_array($request['snuid'], $from_id) ) {
            return false;
        }

        return true;
    }
}
