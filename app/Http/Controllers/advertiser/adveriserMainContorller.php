<?php

namespace App\Http\Controllers\advertiser;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AdUserRepository;
use App\Repositories\Contracts\AdvertisingRepository;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;

class adveriserMainContorller extends Controller
{
    protected $adUser;
    protected $advertising;

    public function __construct(AdUserRepository $adUser, AdvertisingRepository $advertising)
    {
        $this->adUser = $adUser;
        $this->advertising = $advertising;

        $this->getAdUsers();
    }

    public function index()
    {
        $advertising_list = $this->advertising->withCriteria([
            new EagerLoad(['adUsers'])
        ])->Paginate(15);


        return view('advertiser.main', compact( 'advertising_list'));
    }

    public function adUserChange(Request $request)
    {
        $request_data = json_decode($request->data);

        if ($request_data->adUserid) {
            $users = $this->adUser->find($request_data->adUserid);

            if ($users) {
                $request->session()->put('adUserss_id', $users->id);
                $request->session()->put('adUserss_user_id', $users->user_id);
            } else {
                $request->session()->forget('adUserss_id');
                $request->session()->forget('adUserss_user_id');
            }
        } else {
            $request->session()->forget('adUserss_id');
            $request->session()->forget('adUserss_user_id');
        }

        $result['result'] = "success";
        $response = response()->json($result, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);

        return $response;
    }
}
