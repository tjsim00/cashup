<?php

namespace App\Http\Controllers\advertiser;

use App\ApiAccounts;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AdUserRepository;
use App\Repositories\Contracts\ReportRepository;
use Illuminate\Http\Request;

class adreportController extends Controller
{
    protected $adUser;
    protected $report;

    public function __construct(AdUserRepository $adUser, ReportRepository $report)
    {
        $this->adUser = $adUser;
        $this->getAdUsers();
        $this->report = $report;
    }

    public function Monthly(Request $request)
    {
        return view('advertiser.report.monthly');
    }

    public function Daily(Request $request)
    {
        return view('advertiser.report.donthly');
    }

    public function Hourly(Request $request)
    {
        return view('advertiser.report.hourly');
    }

    public function ByMedia()
    {
        return view('advertiser.report.bymedia');
    }

    public function ByAdvertiser()
    {
        return view('advertiser.report.byadvertiser');
    }

    public function ByPeriod(Request $request)
    {
        if (isset($request->sch)) {
            $date = explode(" ~ ", $request->sch);
            $start_date = $date[0];
            $end_date = $date[1];
        } else {
            $start_date = date("Y-m-d");
            $end_date = date("Y-m-d");
        }

        $results = ApiAccounts::get();

        $date_range = $this->date_range($start_date, $end_date, "+1 day", "Y-m-d");

        $data = array_map(function ($item) use ($results) {
            $taproad_point = $results->sum(function ($results_item) use ($item) {
                if ($item == date("Y-m-d", strtotime($results_item->created_at)) && $results_item->advertising_name == "taproad") {
                    if (is_numeric($results_item->user_point)) {
                        return $results_item->user_point;
                    }
                }
            });


            $tapjoy_point = $results->sum(function ($results_item) use ($item) {
                if ($item == date("Y-m-d", strtotime($results_item->created_at)) && $results_item->advertising_name == "tapjoy") {
                    if (is_numeric($results_item->user_point)) {
                        return $results_item->user_point;
                    }
                }
            });


            return ['date'=>$item, 'taproad_point'=>$taproad_point, 'tapjoy_point'=>$tapjoy_point];
        }, $date_range);

        $total['taproad_point'] = 0;
        $total['tapjoy_point'] = 0;
        $total['taproad_count'] = 0;
        $total['tapjoy_count'] = 0;

        foreach ($data as $key => $value) {
            $datas[] = [
                "date" => $value['date'],
                "taproad_point" => $value['taproad_point'],
                "tapjoy_point" => $value['tapjoy_point'],
                "quantity"=> $value['taproad_point']+$value['tapjoy_point']
            ];

            $chat_datas[$value['date']] = [
                    "taproad_point" => $value['taproad_point'],
                    "tapjoy_point" => $value['tapjoy_point'],
                    "quantity"=> $value['taproad_point']+$value['tapjoy_point']
            ];

            $total['taproad_point'] = (int) $total['taproad_point'] + (int) $value['taproad_point'];
            $total['tapjoy_point'] = (int) $total['tapjoy_point'] + (int) $value['tapjoy_point'];
            $total['taproad_count'] = (int) $total['taproad_count'] + (int) $value['taproad_count'];
            $total['tapjoy_count'] = (int) $total['tapjoy_count'] + (int) $value['tapjoy_count'];
        }

        $table_date = $start_date;

        $chart_width = 100;

        if (count($chat_datas) > 8) {
            $chart_width = count($chat_datas) * 16;
        }

        return view('advertiser.report.byperiod', compact('datas','chat_datas','start_date', 'end_date', 'table_date','chart_width', 'total'));
    }

    public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' )
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while( $current <= $last ) {
            //$dates[] = date($output_format, $current);
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }
}
