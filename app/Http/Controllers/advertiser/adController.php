<?php

namespace App\Http\Controllers\advertiser;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\{AdUserRepository, AdvertisingRepository};
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class adController extends Controller
{
    protected $adUser;
    protected $advertising;

    public function __construct(AdUserRepository $adUser, AdvertisingRepository $advertising)
    {
        $this->adUser = $adUser;
        $this->advertising = $advertising;

        $this->getAdUsers();
    }

    public function index(Request $request)
    {
        $ad_id = $request->session()->get('adUserss_id');
        $advertising_list = $this->advertising->withCriteria([
            new EagerLoad(['adUsers'])
        ])->findWherePaginate('ad_id', $ad_id, 15);

        return view('advertiser.ad.index', compact( 'advertising_list'));
    }

    public function create()
    {
        return view('advertiser.ad.create');
    }

    public function store(Request $request)
    {
        $create_result = $this->advertising->create([
            'ad_id' => $request->session()->get('adUserss_id'),
            'setting' => $request['setting'],
            'formality' => $request['formality'],
            'title' => $request['title'],
            'content' => $request['content'],
            'division' => $request['division'],
            'price' => $request['price'],
            'url' => $request['url'],
            'stutus' => $request['stutus'],
            'period' => $request['period'],
            'restriction' => $request['restriction'],
        ]);

        $advertising_id = $create_result->id;

        if (!is_null($request->file('ori_image'))) {
            $filenamewithextension = $request->file('ori_image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('ori_image')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $thumbnail = $filename.'_'.time().'_thumbnail'.'.'.$extension;
            $request->file('ori_image')->storeAs('public/advertising/'.$advertising_id, $filenametostore);
            $request->file('ori_image')->storeAs('public/advertising/'.$advertising_id.'/thumbnail', $thumbnail);

            $thumbnailpath = public_path('storage/advertising/'.$advertising_id.'/thumbnail/'.$thumbnail);
            $this->createThumbnail($thumbnailpath, 150, 93);


            $this->advertising->update($advertising_id, [
                'ori_image' => $filenamewithextension,
                'thum_image' => $thumbnail,
            ]);
        }


        return redirect()->route('adsetup.index')->withSuccess(['Success Message here!']);
    }

    public function edit($id)
    {
        $advertising_list = $this->advertising->withCriteria([
            new EagerLoad(['adUsers'])
        ])->find($id);

        return view('advertiser.ad.edit', compact('advertising_list'));
    }

    public function update(Request $request, $id)
    {
        $update_result = $this->advertising->update($id, [
            'ad_id' => $request->session()->get('adUserss_id'),
            'setting' => $request['setting'],
            'formality' => $request['formality'],
            'title' => $request['title'],
            'content' => $request['content'],
            'division' => $request['division'],
            'price' => $request['price'],
            'url' => $request['url'],
            'stutus' => $request['stutus'],
            'period' => $request['period'],
            'restriction' => $request['restriction'],
        ]);

        if (!is_null($request->file('ori_image'))) {
            Storage::deleteDirectory('public/advertising/'.$id);

            $filenamewithextension = $request->file('ori_image')->getClientOriginalName();
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension = $request->file('ori_image')->getClientOriginalExtension();
            $filenametostore = $filename.'_'.time().'.'.$extension;
            $thumbnail = $filename.'_'.time().'_thumbnail'.'.'.$extension;
            $request->file('ori_image')->storeAs('public/advertising/'.$id, $filenametostore);
            $request->file('ori_image')->storeAs('public/advertising/'.$id.'/thumbnail', $thumbnail);

            $thumbnailpath = public_path('storage/advertising/'.$id.'/thumbnail/'.$thumbnail);
            $this->createThumbnail($thumbnailpath, 150, 93);

            $this->advertising->update($id, [
                'ori_image' => $filenamewithextension,
                'thum_image' => $thumbnail,
            ]);

        }

        return redirect()->route('adsetup.index')->withSuccess(['Success Message here!']);
    }

    public function destroy($id)
    {
        $this->advertising->delete($id);

        return redirect()->route('adsetup.index')->withSuccess(['Success Message here!']);
    }

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
    }
}
