<?php

namespace App\Http\Controllers\secure;

use App\ApiKdiskCallbacks;
use App\Http\Controllers\Controller;
use App\Lib\Response;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class kdiskController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new Response();
    }

    public function test(Request $request)
    {
        $param['cam_id'] = $request['cam_id'] ?? "18911";
        $param['inv_id'] = $request['inv_id'] ?? "kdisk";
        $param['inv_hash'] = $request['inv_hash'] ?? "TESTER01";
        $param['uniqcode'] = $request['uniqcode'] ?? date("Ym")."_".mt_rand(1000000000000, 9999999999999);
        $param['cam_nm'] = urlencode($request['cam_nm'] ?? "Finnq APP [최초구동]");
        $param['pointval'] = $request['pointval'];


        $output = implode('&', array_map(
            function ($v, $k) {
                if(is_array($v)){
                    return $k.'[]='.implode('&'.$k.'[]=', $v);
                }else{
                    return $k.'='.$v;
                }
            },
            $param,
            array_keys($param)
        ));

        $url = $request['url']."?".$output;

        $this->setPointByMd($param, $url);

        $json_data = Curl::to($url)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->post();

        return view('secure.kdisk', compact('request', 'json_data', 'url','output'));
    }

    public function setPointByMd($param, $url)
    {
        ApiKdiskCallbacks::create([
            'cam_id'              =>  $param['cam_id'],
            'inv_id'              =>  $param['inv_id'],
            'inv_hash'            =>  $param['inv_hash'],
            'uniqcode'            =>  $param['uniqcode'],
            'cam_nm'              =>  $param['cam_nm'],
            'pointval'            =>  $param['pointval'],
            'url'                 =>  $url,
        ]);
    }
}

