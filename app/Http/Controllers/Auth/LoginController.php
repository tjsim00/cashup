<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:master')->except('logout');
        $this->middleware('guest:ad')->except('logout');
        $this->middleware('guest:md')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'name';
    }

    public function showMasterLoginForm()
    {
        return view('auth.master_login');
    }

    public function masterLogin(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('master')->attempt(['user_id' => $request->user_id, 'password' => $request->password, 'user_type' => Config::get('user_type')], $request->get('remember'))) {

            return redirect()->intended('/masteruser');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function showAdLoginForm()
    {
        return view('auth.ad_login');
    }

    public function adLogin(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('ad')->attempt(['user_id' => $request->user_id, 'password' => $request->password, 'user_type' => Config::get('user_type')], $request->get('remember'))) {
            $request->session()->put('adUserss_id', Auth::guard('ad')->user()->id);
            $request->session()->put('adUserss_user_id', Auth::guard('ad')->user()->user_id);

            return redirect()->intended(route('adsetup.index'));
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    public function showMdLoginForm()
    {
        return view('auth.md_login');
    }

    public function mdLogin(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('md')->attempt(['user_id' => $request->user_id, 'password' => $request->password, 'user_type' => Config::get('user_type')], $request->get('remember'))) {
            $request->session()->put('mdUserss_id', Auth::guard('md')->user()->id);
            $request->session()->put('mdUserss_user_id', Auth::guard('md')->user()->user_id);

            return redirect()->intended(route('md.monthly'));
        }

        return back()->withInput($request->only('email', 'remember'));
    }


}
