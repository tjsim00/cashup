<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:master');
        $this->middleware('guest:ad');
        $this->middleware('guest:md');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showMasterRegisterForm()
    {
        return view('auth.master_register');
    }

    public function createMaster(Request $request)
    {
        $data = $this->validator($request->all())->validate();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => 'master',
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->intended('login/master');
    }

    public function showAdRegisterForm()
    {
        return view('auth.ad_register');
    }

    public function createAd(Request $request)
    {
        $data = $this->validator($request->all())->validate();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => 'ad',
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->intended('login/ad');
    }

    public function showMdRegisterForm()
    {
        return view('auth.md_register');
    }

    public function createMd(Request $request)
    {
        $data = $this->validator($request->all())->validate();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => 'md',
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->intended('login/md');
    }
}
