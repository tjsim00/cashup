<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\Lib\Response;


class InboundController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new Response();
    }

    public function list(Request $request)
    {
        $url = "http://taproad.beezplanet.co.kr/inbound/list.php?affiliate=".$request->affiliate."&id=".$request->id;
        $json_data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        $json_data->content = preg_replace("/http:/","/https:/",$json_data->content);
        $json_data->content = str_replace("/https:/","https:/",$json_data->content);
        $json_data->content = str_replace("https:/\/\/taproad.beezplanet.co.kr", "http:/\/\/taproad.beezplanet.co.kr", $json_data->content);
        $json_data->content = str_replace("https:/\/\/image.aline-soft.kr", "http:/\/\/image.aline-soft.kr", $json_data->content);


        // 결과 값이 404, 500 일 때 처리
        if ($json_data->status == "200") {
            $response_data = $json_data->content;
        } else if ($json_data->status == "404") {
            $response_data = $this->response->set_response(9002, null);
        } else if ($json_data->status == "500") {
            $response_data = $this->response->set_response(9003, null);
        }

        return $response_data;
    }

    function getRealClientIp() {
        $ipaddress = '';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if(isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if(isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = null;
        }

        return $ipaddress;
    }

    public function ip(Request $request)
    {
        $url = "http://taproad.beezplanet.co.kr/inbound/ip.php";
        $json_data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        return $this->getRealClientIp();
    }

    public function entry(Request $request)
    {
        $url = "http://taproad.beezplanet.co.kr/inbound/entry.php?supplier=".$request->supplier."&campaign_key=".$request->campaign_key."&affiliate=".$request->affiliate."&usn=".$request->usn."&telecom=".$request->telecom."&ip=".$request->ip."&adid=".$request->adid."&imei=".$request->imei."&model=".$request->model."&ntype=".$request->ntype."&gid=".$request->gid."&api=".$request->api."";
        $json_data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        if ($json_data->status == "200") {
            $temp = json_decode($json_data->content);
            if (preg_match('/market:\/\/details/', $temp->RedirectURL)) {
                $temp->RedirectURL = preg_replace('/market:\/\/details/','https://play.google.com/store/apps/details',$temp->RedirectURL);
            }
            $json_data->content = json_encode($temp) ;
            $response_data = $json_data->content;

        } else if ($json_data->status == "404") {
            $response_data = $this->response->set_response(9002, null);
        } else if ($json_data->status == "500") {
            $response_data = $this->response->set_response(9003, null);
        }

        return $response_data;
    }

    public function apppackage(Request $request)
    {
        $url = "http://taproad.beezplanet.co.kr/inbound/apppackage.php?supplier=".$request->supplier."&campaign_key=".$request->campaign_key."";
        $json_data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        if ($json_data->status == "200") {
            $response_data = $json_data->content;
        } else if ($json_data->status == "404") {
            $response_data = $this->response->set_response(9002, null);
        } else if ($json_data->status == "500") {
            $response_data = $this->response->set_response(9003, null);
        }

        return $response_data;
    }

    public function appinstalled(Request $request)
    {
        $url = "http://taproad.beezplanet.co.kr/inbound/appinstalled.php?supplier=".$request->supplier."&campaign_key=".$request->campaign_key."&affiliate=".$request->affiliate."&usn=".$request->usn."&ip=".$request->ip."&adid=".$request->adid."";
        $json_data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        if ($json_data->status == "200") {
            $response_data = $json_data->content;
        } else if ($json_data->status == "404") {
            $response_data = $this->response->set_response(9002, null);
        } else if ($json_data->status == "500") {
            $response_data = $this->response->set_response(9003, null);
        }

        return $response_data;
    }
}
