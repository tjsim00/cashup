<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Lib\Response;
use App\Repositories\Contracts\ApiLogRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class logController extends Controller
{
    protected $response;
    protected $apilog;

    public function __construct(ApiLogRepository $apilog)
    {
        $this->response = new Response();
        $this->apilog = $apilog;
    }
    public function log(Request $request)
    {
        try {
            $validator = $this->validate($request, [
                'live'         =>  'nullable|string',
                'event'        =>  'required|string',
                'type'         =>  'required|string',
                'af'           =>  'required|string',
                'aid'          =>  'required|string',
                'ad_id'        =>  'required|string',
            ]);

            $live = isset($validator['live']) ? $validator['live'] : null ;

            $this->apilog->create([
                'live'         =>  $live,
                'event'        =>  $validator['event'],
                'type'         =>  $validator['type'],
                'af'           =>  $validator['af'],
                'aid'          =>  $validator['aid'],
                'ad_id'        =>  $validator['ad_id'],
            ]);

        } catch (ValidationException $e) {
            return $this->response->set_response(1001, null);
        }

        dd($validator);

    }
}
