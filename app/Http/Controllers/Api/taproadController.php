<?php

namespace App\Http\Controllers\Api;


use App\ApiKdiskCallbacks;
use App\Http\Controllers\Controller;
use App\Lib\Response;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Ixudra\Curl\Facades\Curl;

class taproadController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new Response();
    }

    public function callback(Request $request)
    {
        $AppKey = "cash.up.200710";

        $this->apiLogSave(session()->getId(), 'in', 'taproad','','request', $request->getQueryString(),'taproad_callback','200');

        $info['AppKey'] = $AppKey;
        $info['session_id'] = session()->getId();
        $info['media_id'] = "kdisk";
        $info['advertising_id'] = "taproad";
        $info['advertising_name'] = "taproad";

        try {
            $validator = $this->validate($request, [
                'id'                    =>  'required|string',
                'sec_k'                 =>  'required|string',
                'ad_idx'                =>  'required|numeric',
                'reward_quantity'       =>  'required|numeric',
                'reward_name'           =>  'required|string',
            ]);

        } catch (ValidationException $e) {
            $this->apiLogSave(session()->getId(), 'out', '','taproad','response', $e,'taproad_callback','403');

            $info['status'] = "faill";
            $info['message'] = $e;
            $this->saveAccount($request, $info);

            return $this->response->set_response(1001, null);
        }

        $verifyCode = md5($AppKey . $request['id'] . $request['ad_idx']);

        if ($request['sec_k'] == null || $verifyCode != $request['sec_k']) {
            $result['Ref'] = "F";
            $result['Message'] = "verifyCode != SecurityKey";

            $info['status'] = "faill";
            $info['message'] = $result['Message'];

        } else {
            if ($request['sec_k'] == null || $verifyCode != $request['sec_k']) {
                $result['Ref'] = "F";
                $result['Message'] = "verifyCode != SecurityKey";

                $info['status'] = "faill";
                $info['message'] = $result['Message'];

            } else {
                $result['Ref'] = "S";
                $request['Message'] = "success";

                $info['status'] = "success";
                $info['message'] = "";
            }
        }
        $this->apiLogSave(session()->getId(), 'out', '','taproad','response', $result['Ref'],'taproad_callback','200');

        $info['id'] =  $request['id'];
        $info['reward_quantity'] = $request['reward_quantity'];
        $info['ad_idx'] = $request['ad_idx'];
        $info['reward_name'] = $request['reward_name'];

        $this->saveAccount($request, $info);

        if ($this->exception($request)) {
            $this->kdiskSend($request, $info);
        }

        return response()->json($result, 200);
    }

}
