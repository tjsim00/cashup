<?php

namespace App\Http\Controllers\Api;

use App\ApiAccounts;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PointCallbackRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Lib\Response;
use Illuminate\Validation\ValidationException;

class pointController extends Controller
{
    protected $response;
    protected $pointcallback;

    public function __construct(PointCallbackRepository $pointcallback)
    {
        $this->response = new Response();
        $this->pointcallback = $pointcallback;
    }

    public function callback(Request $request)
    {

        try {
            $validator = $this->validate($request, [
                'call_type'           =>  'required|string',
                'call_point'          =>  'required|string',
                'call_adid'           =>  'required|string',
                'call_cid'            =>  'required|string',
                'call_gid'            =>  'required|string',
                'call_af'             =>  'required|string',
                'call_aid'            =>  'required|string',
                'call_token'          =>  'required|string',
            ]);

            $this->pointcallback->create([
                'call_type'           =>  $validator['call_type'],
                'call_point'          =>  $validator['call_point'],
                'call_adid'           =>  $validator['call_adid'],
                'call_cid'            =>  $validator['call_cid'],
                'call_gid'            =>  $validator['call_gid'],
                'call_af'             =>  $validator['call_af'],
                'call_aid'            =>  $validator['call_aid'],
                'call_token'          =>  $validator['call_token'],
            ]);

        } catch (ValidationException $e) {
            return $this->response->set_response(1001, null);
        }

        return $this->response->set_response(0, null);
    }

    public function userPointHistory(Request $request)
    {
        try {
            $validator = $this->validate($request, [
                'user_id'           =>  'required|string',
                'ad_name'          =>  'required|string',
                'md_name'           =>  'required|string',
            ]);

        } catch (ValidationException $e) {
            return $this->response->set_response(1001, null);
        }

        $result = ApiAccounts::where('user_id', $request['user_id'])
            ->where('advertising_name', $request['ad_name'])
            ->where('media_name', $request['md_name'])
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime("-10 days")))
            ->get();

        if ($result->count() < 1) {
            $param[0]['media_name'] = '';
            $param[0]['advertising_name'] = '';
            $param[0]['user_id'] = '';
            $param[0]['user_point'] = 0;
            $param[0]['cam_name'] = '보상내역이 없습니다.';
            $param[0]['created_date'] = '';
        } else {
            $param = $result->map(function ($item)  {
                $item['user_point'] =  (int) $item['user_point'];
                $item['created_date'] =  $item['created_at']->format('Y-m-d');

                return [$item->only(['media_name','advertising_name','user_id','user_point','cam_name', 'created_date'])];
            })->collapse();
        }

        return $this->response->set_response(0, $param);
    }

}
