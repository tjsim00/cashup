<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Repositories\Contracts\LockscreenContentsRepository;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaPreferencesRepository;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Lib\Response;
use Illuminate\Validation\ValidationException;

class lockscreenController extends Controller
{
    protected $response;
    protected $mdUser;
    protected $mediaPreferences;
    protected $media;
    protected $lockscreencontents;

    public function __construct(MdUserRepository $mdUser,
                                MediaPreferencesRepository $mediaPreferences,
                                MediaRepository $media,
                                LockscreenContentsRepository $lockscreencontents)
    {
        $this->mdUser = $mdUser;
        $this->mediaPreferences = $mediaPreferences;
        $this->media = $media;
        $this->lockscreencontents = $lockscreencontents;
        $this->response = new Response();
    }

    public function contents(Request $request)
    {
        try {
            $validator = $this->validate($request, [
                'af'        =>  'required|string',
                'aid'       =>  'required|string',
                'gid'       =>  'required|string',
                'cid'       =>  'required|string',
            ]);

            $this->lockscreencontents->create([
                'af'        => $validator['af'],
                'aid'       => $validator['aid'],
                'gid'       => $validator['gid'],
                'cid'       => $validator['cid'],
            ]);

        } catch (ValidationException $e) {
            return $this->response->set_response(1001, null);
        }

        $info = $this->media
            ->withCriteria([
                new EagerLoad(['mdUsers', 'adUsers'])
            ])
            ->findwhere('name', $request->af);

        if (isset($info[0])) {
            $code = 0;
            $encrypter = app('Illuminate\Encryption\Encrypter');
            $encrypted_token = $encrypter->encrypt(csrf_token());

            $param['ad_type'] = $info[0]->adUsers->formality;
            $param['ad_url'] = urlencode($info[0]->adUsers->url);
//            $param['ad_url'] = $info[0]->adUsers->url;
            $param['ad_id'] = $info[0]->adUsers->title;
//            $param['ad_token'] = bin2hex(openssl_random_pseudo_bytes(188));
            $param['ad_token'] = $encrypted_token;
            $param['DateTime'] = Carbon::now()->timestamp;
        } else {
            $code = 1001;
            $param = null;
        }

        return $this->response->set_response($code,$param);
    }
}
