<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaPreferencesRepository;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use App\Repositories\Eloquent\Criteria\RelationOfMediaPreferencesByMdUid;
use App\Repositories\Eloquent\Criteria\RelationOfMediaPreferencesByMediaid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Lib\Response;
use Illuminate\Validation\ValidationException;

class preferencesController extends Controller
{
    protected $response;
    protected $mdUser;
    protected $mediaPreferences;
    protected $media;

    public function __construct(MdUserRepository $mdUser, MediaPreferencesRepository $mediaPreferences, MediaRepository $media)
    {
        $this->mdUser = $mdUser;
        $this->mediaPreferences = $mediaPreferences;
        $this->media = $media;
        $this->response = new Response();
    }

    public function setting(Request $request)
    {
        try {
            $validator = $this->validate($request, [
                'ver'       =>  'required|string',
                'af'        =>  'required|string',
                'aid'       =>  'required|string',
                'package'   =>  'required|string',
                'gid'       =>  'required|string',
                'cid'       =>  'required|string',
            ]);

        } catch (ValidationException $e) {
            return $this->response->set_response(1001, null);
        }


        $info = $this->mediaPreferences
            ->withCriteria([
                new RelationOfMediaPreferencesByMediaid($request->af),
                new EagerLoad(['mdUsers', 'medias'])
            ])
            ->all();

        if (isset($info[0])) {
            $code = 0;
            $param['status'] = $info[0]->status;
            $param['updatetime'] = $info[0]->updatetime;
            $param['freepoint'] = $info[0]->freepoint;
            $param['freelimit'] = $info[0]->freelimit;
            $param['freequency'] = $info[0]->freequency;
            $param['DateTime'] = Carbon::now()->timestamp;
        } else {
            $code = 1001;
            $param = null;
        }

//        $code = 0;
//        $param['status'] = "on";
//        $param['updatetime'] = "180";
//        $param['freepoint'] = "2";
//        $param['freelimit'] = "6";
//        $param['freequency'] = "3";
//        $param['DateTime'] = Carbon::now()->timestamp;

        return $this->response->set_response($code, $param);
    }
}
