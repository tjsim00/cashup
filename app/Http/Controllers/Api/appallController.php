<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Lib\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class appallController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new Response();
    }

    public function callback(Request $request)
    {
        $AppKey = "ab01996d201ab6340463bac785e24e3f26b7e575";

        $this->apiLogSave(session()->getId(), 'in', 'appall','','request', $request->getQueryString(),'appall_callback','200');

        $info['AppKey'] = $AppKey;
        $info['session_id'] = session()->getId();
        $info['media_id'] = "kdisk";
        $info['advertising_id'] = "appall";
        $info['advertising_name'] = "appall";

        try {
            $validator = $this->validate($request, [
                'user_id'               =>  'required|string',
                'point'                 =>  'required|numeric',
                'point_offer'           =>  'required|numeric',
                'device'                =>  'required|string',
                'ad_name'               =>  'required|string',
                'seq_id'                =>  'required|string',
            ]);
        } catch (ValidationException $e) {
            $this->apiLogSave(session()->getId(), 'out', '','appall','response', $e,'appall_callback','403');

            $info['status'] = "faill";
            $info['message'] = $e;
            $this->saveAccount($request, $info);

            return $this->response->set_response(1001, null);
        }
        $message = "success";
        $status = 200;

        $info['status'] = $message;
        $info['message'] = "";

        $this->apiLogSave(session()->getId(), 'out', '','appall','response', $message,'appall_callback','200');

        $info['id'] =  $request['user_id'];
        $info['reward_quantity'] = $request['point'];
        $info['reward_name'] = $request['ad_name'];

        $ad_idx = $this->saveAccount($request, $info);

        $request['ad_idx'] = $ad_idx;
        $request['reward_name'] = 'appall';
        $request['reward_quantity'] = $request['point'];

        if ($this->exception($request)) {
            $this->kdiskSend($request, $info);
        }

        return response()->json()->header('status', $status);
    }
}
