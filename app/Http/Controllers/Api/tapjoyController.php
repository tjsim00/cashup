<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Lib\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class tapjoyController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new Response();
    }

    public function callback(Request $request)
    {
        $secret_key = "SWDwDvqRuKmTFdKce2lC";
        $this->apiLogSave(session()->getId(), 'in', 'tapjoy','','request', $request->getQueryString(),'tapjoy_callback','200');

        $info['secret_key'] = $secret_key;
        $info['session_id'] = session()->getId();
        $info['media_id'] = "kdisk";
        $info['advertising_id'] = "tapjoy";
        $info['advertising_name'] = "tapjoy";

        try {
            $validator = $this->validate($request, [
                'id'                 => 'required|string',
                'verifier'           => 'required|string',
                'snuid'              => 'required|string',
                'currency'           => 'required|string',
                'mac_address'        => 'nullable|string',
                'display_multiplier' => 'required|string',
            ]);

        } catch (ValidationException $e) {
            $this->apiLogSave(session()->getId(), 'out', '','tapjoy','response', $e,'tapjoy_callback','403');

            $info['status'] = "faill";
            $info['message'] = $e;
            $this->saveAccount($request, $info);

            return response()->json()->header('status', 403);
        }

        $verifyCode = md5($request['id'] .":". $request['snuid'] .":". $request['currency'] .":". $secret_key);

        if ($request['verifier'] == null || $verifyCode != $request['verifier']) {
            $message = "verifyCode != Secret_key";
            $status = 403;

            $info['status'] = "faill";
            $info['message'] = $message;

        } else {
            $message = "success";
            $status = 200;

            $info['status'] = "success";
            $info['message'] = "";
        }

        $this->apiLogSave(session()->getId(), 'out', '','tapjoy','response', $message,'tapjoy_callback',$status);

        $info['id'] =  $request['snuid'];
        $info['reward_quantity'] = $request['currency'];

        $ad_idx = $this->saveAccount($request, $info);

        $request['ad_idx'] = $ad_idx;
        $request['reward_name'] = 'tapjoy';
        $request['reward_quantity'] = $request['currency'];

        if ($this->exception($request)) {
            $this->kdiskSend($request, $info);
        }

        return response()->json()->header('status', $status);
    }


}
