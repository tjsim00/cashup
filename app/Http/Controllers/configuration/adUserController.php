<?php

namespace App\Http\Controllers\configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdUser;
use App\Repositories\Contracts\AdUserRepository;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class adUserController extends Controller
{
    protected $adUser;

    public function __construct(AdUserRepository $adUser)
    {
        $this->adUser = $adUser;
    }

    public function index()
    {
        $users = $this->adUser->withCriteria([
            new ByTypeUser('ad'),
        ])->paginate(15);

        return view('configuration.adUser.index', compact('users'));
    }

    public function create()
    {
        return view('configuration.adUser.create');
    }


    public function store(AdUser $request)
    {
        $this->adUser->create([
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'company_name' => $request['company_name'],
            'business_number' => $request['business_number'],
            'homepage' => $request['homepage'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect()->route('aduser.index')->withSuccess(['Success Message here!']);
    }

    public function edit($id)
    {
        $users = $this->adUser->find($id);

        return view('configuration.adUser.edit', compact('users'));
    }


    public function update(AdUser $request, $id)
    {
        if (is_null($request['password'])) {
            $user = $this->adUser->find($id);
            $password = $user->password;
        } else {
            $password = Hash::make($request['password']);
        }

        $this->adUser->update($id, [
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'company_name' => $request['company_name'],
            'business_number' => $request['business_number'],
            'homepage' => $request['homepage'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => $password,
        ]);

        return redirect()->route('aduser.index')->withSuccess(['Success Message here!']);
    }


    public function destroy($id)
    {
        $this->adUser->delete($id);

        return redirect()->route('aduser.index')->withSuccess(['Success Message here!']);
    }
}
