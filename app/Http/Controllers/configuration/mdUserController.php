<?php

namespace App\Http\Controllers\configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\MdUser;
use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\Contracts\MediaPreferencesRepository;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class mdUserController extends Controller
{
    protected $mdUser;
    protected $mediaPreferences;

    public function __construct(MdUserRepository $mdUser, MediaPreferencesRepository $mediaPreferences)
    {
        $this->mdUser = $mdUser;
        $this->mediaPreferences = $mediaPreferences;
    }

    public function index()
    {
        $users = $this->mdUser->withCriteria([
            new ByTypeUser('md'),
        ])->paginate(15);

        return view('configuration.mdUser.index', compact('users'));
    }

    public function create()
    {
        return view('configuration.mdUser.create');
    }

    public function store(MdUser $request)
    {
        $create_result = $this->mdUser->create([
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'media_name' => $request['media_name'],
            'site_url' => $request['site_url'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect()->route('mduser.index')->withSuccess(['Success Message here!']);
    }

    public function edit($id)
    {
        $users = $this->mdUser->find($id);

        return view('configuration.mdUser.edit', compact('users'));
    }

    public function update(MdUser $request, $id)
    {
        if (is_null($request['password'])) {
            $user = $this->mdUser->find($id);
            $password = $user->password;
        } else {
            $password = Hash::make($request['password']);
        }

        $this->mdUser->update($id, [
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'media_name' => $request['media_name'],
            'site_url' => $request['site_url'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => $password,
        ]);

        return redirect()->route('mduser.index')->withSuccess(['Success Message here!']);
    }

    public function destroy($id)
    {
        $this->mdUser->delete($id);

        return redirect()->route('mduser.index')->withSuccess(['Success Message here!']);
    }
}
