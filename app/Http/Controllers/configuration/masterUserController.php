<?php

namespace App\Http\Controllers\configuration;

use App\Http\Controllers\Controller;

use App\Http\Requests\MasterUser;
use App\Repositories\Contracts\MasterUserRepository;
use App\Repositories\Eloquent\Criteria\ByTypeUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class masterUserController extends Controller
{
    protected $masterUser;

    public function __construct(MasterUserRepository $masterUser)
    {
        $this->masterUser = $masterUser;
    }

    public function index()
    {
//        dd(Auth::guard('master')->user()->name);

        $users = $this->masterUser->withCriteria([
            new ByTypeUser('master'),
        ])->paginate(15);

        return view('configuration.masterUser.index', compact('users'));
    }

    public function create()
    {
        return view('configuration.masterUser.create');
    }

    public function store(MasterUser $request)
    {
        $this->masterUser->create([
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect()->route('masteruser.index')->withSuccess(['Success Message here!']);
    }

    public function edit($id)
    {
        $users = $this->masterUser->find($id);

        return view('configuration.masterUser.edit', compact('users'));
    }

    public function update(MasterUser $request, $id)
    {
        if (is_null($request['password'])) {
            $user = $this->masterUser->find($id);
            $password = $user->password;
        } else {
            $password = Hash::make($request['password']);
        }

        $this->masterUser->update($id, [
            'user_id' => $request['user_id'],
            'name' => $request['name'],
            'user_type' => $request['user_type'],
            'phone' => $request['phone'],
            'mobile_phone' => $request['mobile_phone'],
            'email' => $request['email'],
            'bigo' => $request['bigo'],
            'password' => $password,
        ]);

        return redirect()->route('masteruser.index')->withSuccess(['Success Message here!']);
    }

    public function destroy($id)
    {
        $this->masterUser->delete($id);

        return redirect()->route('masteruser.index')->withSuccess(['Success Message here!']);
    }
}
