<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class DomainCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        list($subdomain) = explode('.', $request->getHost(), 2);

        if (!in_array($subdomain, Config::get('domain') )) {
            abort(403, "Please check the route again.");
            exit;
        }

        Config::set(['user_type' => $subdomain]);

        return $next($request);
    }
}
