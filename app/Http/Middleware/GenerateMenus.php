<?php

namespace App\Http\Middleware;

use Closure;


use Spatie\Menu\Html;
use Spatie\Menu\Link;
use Spatie\Menu\Menu;


class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $master_menu = Menu::new()
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>관리자')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>관리자 등록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>관리자 추가</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            )
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>광고주')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>광고주 등록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>광고주 추가</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            )
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>메체자')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>매체자 등록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>매체자 추가</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            );

//        $ad_menu = Menu::new()
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>광고 관리')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>광고 목록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>광고 등록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            )
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>매체 관리')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>매체 목록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>매체 등록</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            )
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>보고서')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>월별</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>일별</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>시간대 별</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>매체 별</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>광고 별</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            )
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>사용자 LOG')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>사용자 LOG\'</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            );

//        $md_menu = Menu::new()
//            ->addClass('c-sidebar-nav')
//            ->submenu(
//                Link::to('#', '<i class="cil-speedometer c-sidebar-nav-icon"></i>사용자 LOG')->addClass('c-sidebar-nav-link'),
//                Menu::new()
//                    ->add(Html::raw('<a href="#" class="c-sidebar-nav-link"><span class="c-sidebar-nav-icon"></span>사용자 LOG\'</a>')->addParentClass('c-sidebar-nav-item'))
//                    ->addClass('c-sidebar-nav-dropdown-items')->addParentClass('c-sidebar-nav-dropdown')
//            );

//        view()->share('master_menu', $master_menu);
//        view()->share('ad_menu', $ad_menu);
//        view()->share('md_menu', $md_menu);

        return $next($request);
    }
}
