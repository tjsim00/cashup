<?php

namespace App\Http\Middleware;

use Closure;

class adUserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('adUserss_id')) {
            return redirect()->route('adveriser.main');
        }

        return $next($request);
    }
}
