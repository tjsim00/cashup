<?php

namespace App\Http\Requests;


use App\Rules\IsUserTypeByUserIdUnique;
use App\Rules\IsValidPhoneNumberCheck;
use App\Rules\IsValidUserType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MasterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('master')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if( $this->isMethod('post') ) {
            return $this->createRules();
        } elseif ( $this->isMethod('put') ) {
            return $this->updateRules();
        }
    }

    public function createRules()
    {
        return [
            'user_type' => ['required', new IsValidUserType],
            'user_id' => ['required', 'string', new IsUserTypeByUserIdUnique()],
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'password' => ['required', 'min:6', 'same:password_confirm'],
            'password_confirm' => ['required', 'min:6'],
            'phone' => ['required', new IsValidPhoneNumberCheck],
            'mobile_phone' => ['required', new IsValidPhoneNumberCheck],
        ];
    }

    public function updateRules()
    {
        return [
            'user_type' => ['required', new IsValidUserType],
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'password' => ['nullable', 'min:6', 'same:password_confirm'],
            'password_confirm' => ['nullable', 'min:6'],
            'phone' => ['required', new IsValidPhoneNumberCheck],
            'mobile_phone' => ['required', new IsValidPhoneNumberCheck],
        ];
    }

}
