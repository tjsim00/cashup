<?php

namespace App\Providers;

use App\Repositories\Contracts\{AdUserRepository,
    AdvertisingRepository,
    ApiLogRepository,
    LockscreenContentsRepository,
    MasterUserRepository,
    MdUserRepository,
    MediaPreferencesRepository,
    MediaRepository,
    PointCallbackRepository,
    ReportRepository,
    TopicRepository};

use App\Repositories\Eloquent\{EloquentAdUserRepository,
    EloquentAdvertisingRepository,
    EloquentApiLogRepository,
    EloquentLockscreenContentsRepository,
    EloquentMasterUserRepository,
    EloquentMdUserRepository,
    EloquentMediaPreferencesRepository,
    EloquentMediaRepository,
    EloquentPointCallbackRepository,
    EloquentReportRepository,
    EloquentTopicRepository};

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(TopicRepository::class, EloquentTopicRepository::class);
        $this->app->bind(MasterUserRepository::class, EloquentMasterUserRepository::class);
        $this->app->bind(AdUserRepository::class, EloquentAdUserRepository::class);
        $this->app->bind(MdUserRepository::class, EloquentMdUserRepository::class);
        $this->app->bind(AdvertisingRepository::class, EloquentAdvertisingRepository::class);
        $this->app->bind(MediaRepository::class, EloquentMediaRepository::class);
        $this->app->bind(MediaPreferencesRepository::class, EloquentMediaPreferencesRepository::class);
        $this->app->bind(ReportRepository::class, EloquentReportRepository::class);
        $this->app->bind(LockscreenContentsRepository::class, EloquentLockscreenContentsRepository::class);
        $this->app->bind(PointCallbackRepository::class, EloquentPointCallbackRepository::class);
        $this->app->bind(ApiLogRepository::class, EloquentApiLogRepository::class);
    }
}
