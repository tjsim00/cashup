<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsValidPhoneNumberCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $is_rule = false;
        $re_phoneNum = preg_replace('/-/', '', $value);

        $mobile = preg_match('/^01[016789]{1}-?([0-9]{3,4})-?[0-9]{4}$/', $value);
        $tel = preg_match('/^(02|0[3-6]{1}[1-5]{1})-?([0-9]{3,4})-?[0-9]{4}$/', $value);
        $rep = preg_match('/^(15|16|18)[0-9]{2}-?[0-9]{4}$/', $value);
        $rep2 = preg_match('/^(02|0[3-6]{1}[1-5]{1})-?(15|16|18)[0-9]{2}-?[0-9]{4}$/', $value);
        $num = preg_match('/^(070|(050[2-8]{0,1})|080|013)-?([0-9]{3,4})-?[0-9]{4}$/', $value);

        if ($mobile != false) {
            $is_rule = true;
            if (strlen($re_phoneNum) > 11) {
                $is_rule = false;
            }
        } else if ($tel != false) {
            $is_rule = true;
            if (strlen($re_phoneNum) > 11) {
                $is_rule = false;
            }
        } else if ($rep != false) {
            $is_rule = true;
            if (strlen($re_phoneNum) != 8) {
                $is_rule = false;
            }
        } else if ($num != false) {
            $is_rule = true;
            if (strlen($re_phoneNum) > 12) {
                $is_rule = false;
            }
        } else {
            $is_rule = false;
        }

        if ($rep2 == true) {
            $is_rule = false;
        }

        return $is_rule;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
