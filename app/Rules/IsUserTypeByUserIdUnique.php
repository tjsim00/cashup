<?php

namespace App\Rules;

use App\Repositories\Eloquent\Criteria\ByTypeUser;
use App\Repositories\Eloquent\Criteria\ByUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;

class IsUserTypeByUserIdUnique implements Rule
{
    protected $request;
    protected $masterUser;
    protected $adUser;
    protected $mdUser;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = App::make('Illuminate\Http\Request');
        $this->masterUser = App::make('App\Repositories\Contracts\MasterUserRepository');
        $this->adUser = App::make('App\Repositories\Contracts\AdUserRepository');
        $this->mdUser = App::make('App\Repositories\Contracts\MdUserRepository');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $is_rule = false;

        if ($this->request->user_type == "master") {
            $cnt = $this->getMasterCnt($value);
        } else if ($this->request->user_type == "ad") {
            $cnt = $this->getAdCnt($value);
        } else if ($this->request->user_type == "md") {
            $cnt = $this->getMdCnt($value);
        }

        if ($cnt < 1) {
            $is_rule = true;
        }

        return $is_rule;
    }

    public function getMasterCnt($value)
    {
        $result = $this->masterUser->withCriteria([
            new ByTypeUser($this->request->user_type),
            new ByUser($value),
        ])->all();

        return $result->count();
    }

    public function getAdCnt($value)
    {
        $result = $this->adUser->withCriteria([
            new ByTypeUser($this->request->user_type),
            new ByUser($value),
        ])->all();

        return $result->count();
    }

    public function getMdCnt($value)
    {
        $result = $this->mdUser->withCriteria([
            new ByTypeUser($this->request->user_type),
            new ByUser($value),
        ])->all();

        return $result->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '등록 되어 있는 ID 입니다. 다시 입력해 주세요.';
    }
}
