<?php

namespace App\Lib;

class Response
{
    public function set_response($err_code, $param)
    {

        // Set error code
        $err_data[0] = "success";

        $err_data[1001] = "The given data was invalid.";
        $err_data[9001] = "No data.";
        $err_data[9002] = "No data (404).";
        $err_data[9003] = "No data (500).";

        if ($err_code==0) {
            $result['Result'] = $err_data[$err_code];
            if (is_null($param)) {
                $result['Date'] = (object)[];
            } else {
                $result['Date'] = $param;
            }

        } else {
            $result['Result'] = "error";
            $result['Error'] = $err_data[$err_code];
        }


        return response()->json($result, 200);
    }
}
