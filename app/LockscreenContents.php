<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockscreenContents extends Model
{
    protected $guarded = [];
}
