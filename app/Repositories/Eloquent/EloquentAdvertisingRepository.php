<?php


namespace App\Repositories\Eloquent;


use App\Advertising;
use App\Repositories\Contracts\AdvertisingRepository;
use App\Repositories\RepositoryAbstract;

class EloquentAdvertisingRepository extends RepositoryAbstract implements AdvertisingRepository
{
    public function entity()
    {
        return Advertising::class;
    }
}
