<?php


namespace App\Repositories\Eloquent;


use App\Media;
use App\Repositories\Contracts\MediaRepository;
use App\Repositories\RepositoryAbstract;

class EloquentMediaRepository extends RepositoryAbstract implements MediaRepository
{
    public function entity()
    {
        return Media::class;
    }
}
