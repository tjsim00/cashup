<?php


namespace App\Repositories\Eloquent;


use App\PointCallback;
use App\Repositories\Contracts\PointCallbackRepository;
use App\Repositories\RepositoryAbstract;

class EloquentPointCallbackRepository extends RepositoryAbstract implements PointCallbackRepository
{
    public function entity()
    {
        return PointCallback::class;
    }
}
