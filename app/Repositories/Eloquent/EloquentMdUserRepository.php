<?php


namespace App\Repositories\Eloquent;


use App\Repositories\Contracts\MdUserRepository;
use App\Repositories\RepositoryAbstract;
use App\User;

class EloquentMdUserRepository extends RepositoryAbstract implements MdUserRepository
{
    public function entity()
    {
        return User::class;
    }
}
