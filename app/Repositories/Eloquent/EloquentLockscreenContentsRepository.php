<?php


namespace App\Repositories\Eloquent;


use App\LockscreenContents;
use App\Repositories\Contracts\LockscreenContentsRepository;
use App\Repositories\RepositoryAbstract;

class EloquentLockscreenContentsRepository extends RepositoryAbstract implements LockscreenContentsRepository
{
    public function entity()
    {
        return LockscreenContents::class;
    }
}
