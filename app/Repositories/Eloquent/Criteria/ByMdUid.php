<?php


namespace App\Repositories\Eloquent\Criteria;


class ByMdUid
{
    protected $MdUid;

    public function __construct($MdUid)
    {
        $this->MdUid = $MdUid;
    }

    public function apply($entity)
    {
        return $entity->where('md_uid', $this->MdUid);
    }
}
