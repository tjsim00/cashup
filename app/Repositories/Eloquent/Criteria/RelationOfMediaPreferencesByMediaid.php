<?php


namespace App\Repositories\Eloquent\Criteria;


class RelationOfMediaPreferencesByMediaid
{
    protected $mediaId;

    public function __construct($mediaId)
    {
        $this->mediaId = $mediaId;
    }


    public function apply($entity)
    {
        return $entity->whereHas('medias' , function ($query) {
            $query->where('name', '=', $this->mediaId);
        });
    }
}
