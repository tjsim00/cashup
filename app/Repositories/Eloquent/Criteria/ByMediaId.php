<?php


namespace App\Repositories\Eloquent\Criteria;


class ByMediaId
{
    protected $MediaId;

    public function __construct($MediaId)
    {
        $this->MediaId = $MediaId;
    }

    public function apply($entity)
    {
        return $entity->where('media_id', $this->MediaId);
    }
}
