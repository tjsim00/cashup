<?php


namespace App\Repositories\Eloquent\Criteria;


class ByAdvertisingId
{
    protected $AdvertisingId;

    public function __construct($AdvertisingId)
    {
        $this->AdvertisingId = $AdvertisingId;
    }

    public function apply($entity)
    {
        return $entity->where('advertising_id', $this->AdvertisingId);
    }
}
