<?php


namespace App\Repositories\Eloquent\Criteria;


use App\Repositories\Criteria\CriterionInterface;

class ByTypeUser implements CriterionInterface
{
    protected $userTYPE;

    public function __construct($userTYPE)
    {
        $this->userTYPE = $userTYPE;
    }

    public function apply($entity)
    {
        return $entity->where('user_type', $this->userTYPE);
    }

}
