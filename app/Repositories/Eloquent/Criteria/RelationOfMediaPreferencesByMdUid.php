<?php


namespace App\Repositories\Eloquent\Criteria;


class RelationOfMediaPreferencesByMdUid
{

    protected $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    public function apply($entity)
    {
        return $entity->whereHas('mdUsers' , function ($query) {
            $query->where('user_id', '=', $this->userId);
        });
    }
}
