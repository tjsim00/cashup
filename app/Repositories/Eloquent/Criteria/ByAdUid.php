<?php


namespace App\Repositories\Eloquent\Criteria;


class ByAdUid
{
    protected $AdUid;

    public function __construct($AdUid)
    {
        $this->AdUid = $AdUid;
    }

    public function apply($entity)
    {
        return $entity->where('ad_uid', $this->AdUid);
    }
}
