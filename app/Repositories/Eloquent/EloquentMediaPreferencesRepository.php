<?php


namespace App\Repositories\Eloquent;


use App\MediaPreferences;
use App\Repositories\Contracts\MediaPreferencesRepository;
use App\Repositories\RepositoryAbstract;

class EloquentMediaPreferencesRepository extends RepositoryAbstract implements MediaPreferencesRepository
{
    public function entity()
    {
        return MediaPreferences::class;
    }
}
