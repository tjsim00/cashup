<?php


namespace App\Repositories\Eloquent;


use App\Repositories\Contracts\AdUserRepository;
use App\Repositories\RepositoryAbstract;
use App\User;

class EloquentAdUserRepository extends RepositoryAbstract implements AdUserRepository
{
    public function entity()
    {
        return User::class;
    }
}
