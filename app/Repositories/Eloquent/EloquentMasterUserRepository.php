<?php


namespace App\Repositories\Eloquent;


use App\Repositories\Contracts\MasterUserRepository;
use App\Repositories\RepositoryAbstract;
use App\User;

class EloquentMasterUserRepository extends RepositoryAbstract implements MasterUserRepository
{
    public function entity()
    {
        return User::class;
    }
}
