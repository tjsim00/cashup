<?php


namespace App\Repositories\Eloquent;


use App\Report;
use App\Repositories\Contracts\ReportRepository;
use App\Repositories\RepositoryAbstract;

class EloquentReportRepository extends RepositoryAbstract implements ReportRepository
{
    public function entity()
    {
        return Report::class;
    }
}
