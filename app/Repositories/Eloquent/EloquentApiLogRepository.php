<?php


namespace App\Repositories\Eloquent;


use App\ApiLog;
use App\Repositories\Contracts\ApiLogRepository;
use App\Repositories\RepositoryAbstract;

class EloquentApiLogRepository extends RepositoryAbstract implements ApiLogRepository
{
    public function entity()
    {
        return ApiLog::class;
    }
}
