<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'user_type',
        'phone',
        'mobile_phone',
        'company_name',
        'business_number',
        'homepage',
        'media_name',
        'site_url',
        'bigo',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function mediaPreferences()
    {
        return $this->hasMany(MediaPreferences::class, 'md_uid', 'id');
    }

    public function advertisings()
    {
        return $this->hasMany(Advertising::class, 'ad_id','id');
    }

    public function medias()
    {
        return $this->hasMany(Media::class, 'md_id', 'id');
    }



}
