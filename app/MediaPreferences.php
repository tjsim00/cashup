<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaPreferences extends Model
{
    protected $guarded = [];

    public function mdUsers()
    {
        return $this->hasMany(User::class, 'id', 'md_uid');
    }



    public function medias()
    {
        return $this->hasMany(Media::class, 'id', 'media_id');
    }

}
