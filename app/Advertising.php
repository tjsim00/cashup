<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    protected $guarded = [];

    public function adUsers()
    {
        return $this->belongsTo(User::class, 'ad_id', 'id');
    }
}
