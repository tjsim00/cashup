<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Report;
use Faker\Generator as Faker;

$factory->define(Report::class, function (Faker $faker) {
    $ad_uid = App\User::where('user_type', 'ad')->get()->random()->id;
    $md_uid = App\User::where('user_type', 'md')->get()->random()->id;

    if (App\Advertising::where('ad_id', $ad_uid)->get()->count() > 0) {
        $advertising_id = App\Advertising::where('ad_id', $ad_uid)->get()->random()->id;
    } else {
        $advertising_id = 0;
    }

    if (App\Media::where('md_id', $md_uid)->get()->count() > 0) {
        $media_id = App\Advertising::where('ad_id', $md_uid)->get()->random()->id;
    } else {
        $media_id = 0;
    }
    $clecks = $faker->numberBetween(100, 1000);
    $success = $faker->numberBetween(100, $clecks);
    $jeonhwan = ($success / $clecks) * 100;
    //$cleck_datetime = $faker->dateTimeBetween($startDate = '-1 month', $endDate = 'now');
    $cleck_datetime = $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now');
    return [
        'ad_uid' => $ad_uid,
        'advertising_id' => $advertising_id,
        'md_uid' => $md_uid,
        'media_id' => $media_id,
        'clecks' => $clecks,
        'success' => $success,
        'jeonhwan' => $jeonhwan,
        'cost' => $faker->numberBetween(1000, 10000),
        'point' => $faker->numberBetween(10, 100),
        'cleck_datetime' => $cleck_datetime
    ];
});
