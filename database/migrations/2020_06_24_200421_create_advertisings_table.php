<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisings', function (Blueprint $table) {
            $table->id();
            $table->integer('ad_id')->unsigned()->index();
            $table->string('setting')->comment('설정');
            $table->string('formality')->comment('형식');
            $table->string('title')->comment('제목');
            $table->text('content')->nullable()->comment('내용');
            $table->string('division')->comment('구분');
            $table->string('price')->comment('가격');
            $table->string('url')->comment('url');
            $table->string('stutus')->comment('상태');
            $table->string('period')->comment('기간');
            $table->string('restriction')->comment('제한설정');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisings');
    }
}
