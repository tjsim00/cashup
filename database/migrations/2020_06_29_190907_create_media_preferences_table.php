<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_preferences', function (Blueprint $table) {
            $table->id();
            $table->integer('md_uid')->unsigned()->index()->comment('매체 user_id');
            $table->integer('media_id')->unsigned()->comment('매체 media_id');
            $table->string('status')->default('OFF')->comment('기능 ON/OFF');
            $table->integer('updatetime')->default(0)->comment('데이터 업데이트 갱신시간');
            $table->integer('freepoint')->default(0)->comment('1회에 지급되는 기본 포인트');
            $table->integer('freelimit')->default(0)->comment('1일에 지급되는 총 기본 포인트');
            $table->integer('freequency')->default(0)->comment('기본포인트 지급주기 : 3번 락 풀때 1번 지급');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_preferences');
    }
}
