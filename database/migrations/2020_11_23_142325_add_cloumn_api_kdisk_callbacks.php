<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCloumnApiKdiskCallbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_kdisk_callbacks', function (Blueprint $table) {
            $table->string('session_id')->nullable()->comment('session_id')->after('id');
            $table->text('url')->nullable()->change();
            $table->string('status')->nullable()->comment('status')->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
