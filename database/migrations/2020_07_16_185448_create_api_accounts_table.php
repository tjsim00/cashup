<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_accounts', function (Blueprint $table) {
            $table->id();
            $table->integer('media_id')->unsigned()->default(0)->comment('매체 media_id');
            $table->string('media_name')->comment('매체 media_id');
            $table->integer('advertising_id')->unsigned()->default(0)->comment('광고 advertising_id');
            $table->string('advertising_name')->comment('광고 advertising_id');
            $table->string('user_id')->comment('사용자 id');
            $table->string('user_point')->comment('사용자 id');
            $table->string('cam_id')->comment('cam_id');
            $table->string('cam_name')->comment('cam_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_accounts');
    }
}
