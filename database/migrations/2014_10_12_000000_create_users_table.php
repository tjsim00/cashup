<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->unique()->comment('아이디');
            $table->string('name')->comment('이름, 담당자명');
            $table->string('user_type')->comment('유저타입');;
            $table->string('phone')->nullable()->comment('전화번호');;
            $table->string('mobile_phone')->nullable()->comment('휴대폰번호');;
            $table->string('company_name')->nullable()->comment('회사명');;
            $table->string('business_number')->nullable()->comment('사업자등록번호');;
            $table->string('homepage')->nullable()->comment('홈페이지');;
            $table->string('media_name')->nullable()->comment('매체명');;
            $table->string('site_url')->nullable()->comment('사이트 url');;
            $table->text('bigo')->nullable()->comment('비고');;
            $table->string('email')->nullable()->comment('이메일');;
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->comment('비밀번호');;
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
