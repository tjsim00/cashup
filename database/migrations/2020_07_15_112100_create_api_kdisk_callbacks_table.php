<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiKdiskCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_kdisk_callbacks', function (Blueprint $table) {
            $table->id();
            $table->string('cam_id')->comment('광고코드');
            $table->string('inv_id')->comment('매체코드');
            $table->string('inv_hash')->comment('회원아이디');
            $table->string('uniqcode')->comment('거래번호 (트랜잭션 번호)');
            $table->string('cam_nm')->comment('캠페인명');
            $table->string('pointval')->comment('지급할 포인트');
            $table->string('url')->comment('적립금 지급 URL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_kdisk_callbacks');
    }
}
