<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCloumnApiAccountsDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('api_accounts_details', function (Blueprint $table) {
            $table->integer('point')->nullable()->comment('[appall]point')->after('display_multiplier');
            $table->integer('point_offer')->nullable()->comment('[appall]point_offer')->after('point');
            $table->string('device')->nullable()->comment('[appall]device')->after('point_offer');
            $table->string('ad_name')->nullable()->comment('[appall]ad_name')->after('device');
            $table->string('seq_id')->nullable()->comment('[appall]seq_id')->after('ad_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
