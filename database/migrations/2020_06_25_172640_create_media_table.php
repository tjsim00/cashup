<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->integer('md_id')->unsigned()->index();
            $table->integer('ad_id')->unsigned()->comment('광고주');
            $table->string('name')->comment('매체이름');;
            $table->string('site_url')->comment('사이트url');;
            $table->string('biyul_ad')->comment('비율 광고주');
            $table->string('biyul_md')->comment('비율 매체');
            $table->string('exposure')->comment('노출여부');
            $table->string('adrestriction_lock_screen')->comment('광고기능제한 lock screen');
            $table->string('adrestriction_noti')->comment('광고기능제한 noti');
            $table->text('bigo')->nullable()->comment('비고');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
