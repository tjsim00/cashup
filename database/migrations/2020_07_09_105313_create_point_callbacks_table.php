<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_callbacks', function (Blueprint $table) {
            $table->id();
            $table->string('call_type')->comment('free / offerwall');
            $table->integer('call_point')->comment('지급 포인트');
            $table->string('call_adid')->comment('광고고유아이디 : tabroad / cashup');
            $table->string('call_cid')->comment('클라이언트 내부 아이디 : gid 와 비교');
            $table->string('call_af')->comment('매체사');
            $table->string('call_aid')->comment('매체사 회원아이디');
            $table->string('call_token')->comment('유효성 토큰');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_callbacks');
    }
}
