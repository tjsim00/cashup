<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiBoundLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_bound_logs', function (Blueprint $table) {
            $table->id();
            $table->string('session_id')->nullable()->comment('session_id');
            $table->string('kind')->comment('in/out');
            $table->string('from')->nullable()->comment('from');
            $table->string('to')->nullable()->comment('to');
            $table->string('type')->comment('request/reponse');
            $table->text('paramter')->nullable()->comment('paramter');
            $table->string('method')->nullable()->comment('method');
            $table->string('status')->nullable()->comment('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_bound_logs');
    }
}
