<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('live')->nullable()->comment('live 유무 : 1 / 0');
            $table->string('event')->comment('lockscreen / click');
            $table->string('type')->comment('offerwall / web');
            $table->string('af')->comment('매체사');
            $table->string('aid')->comment('매체사 회원아이디');
            $table->string('ad_id')->comment('광고 고유 아이디');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_logs');
    }
}
