<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiAccountsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_accounts_details', function (Blueprint $table) {
            $table->id();
            $table->string('account_id')->nullable()->comment('account_id');
            $table->string('session_id')->nullable()->comment('session_id');
            $table->string('AppKey')->nullable()->comment('[taproad]Affiliate별 임의의 키 값');
            $table->string('user_id')->nullable()->comment('[taproad]매체 회원의 아이디 혹은 고유 값');
            $table->string('sec_k')->nullable()->comment('[taproad]보안 키 값');
            $table->integer('ad_idx')->nullable()->comment('[taproad]탭로드 내 해당 광고 번호');
            $table->integer('reward_quantity')->nullable()->comment('[taproad]리워드 액수');
            $table->string('reward_name')->nullable()->comment('[taproad]해당 광고 명칭');
            $table->string('Ref')->nullable()->comment('[taproad]호출 성공여부');
            $table->string('int_id')->nullable()->comment('[tapjoy]id');
            $table->string('verifier')->nullable()->comment('[tapjoy]verifier');
            $table->string('snuid')->nullable()->comment('[tapjoy]snuid');
            $table->string('currency')->nullable()->comment('[tapjoy]currency');
            $table->string('mac_address')->nullable()->comment('[tapjoy]mac_address');
            $table->string('display_multiplier')->nullable()->comment('[tapjoy]display_multiplier');
            $table->text('message')->nullable()->comment('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_accounts_details');
    }
}
