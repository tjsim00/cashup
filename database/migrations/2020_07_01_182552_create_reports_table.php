<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->integer('ad_uid')->unsigned()->default(0)->comment('광고 user_id');
            $table->integer('advertising_id')->unsigned()->default(0)->comment('광고 advertising_id');
            $table->integer('md_uid')->unsigned()->default(0)->comment('매체 user_id');
            $table->integer('media_id')->unsigned()->default(0)->comment('매체 media_id');
            $table->integer('clecks')->unsigned()->default(0)->comment('클릭');
            $table->integer('success')->unsigned()->default(0)->comment('성공');
            $table->integer('jeonhwan')->unsigned()->default(0)->comment('전환율');
            $table->integer('cost')->unsigned()->default(0)->comment('매체비');
            $table->integer('point')->unsigned()->default(0)->comment('포인트');
            $table->dateTime('cleck_datetime')->useCurrent()->comment('클릭 시간');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
