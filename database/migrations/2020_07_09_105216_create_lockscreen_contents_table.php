<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLockscreenContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lockscreen_contents', function (Blueprint $table) {
            $table->id();
            $table->string('af')->comment('매체사');
            $table->string('aid')->comment('매체사 회원아이디');
            $table->string('gid')->comment('구글 광고ID');
            $table->string('cid')->comment('클라이언트 내부아이디');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lockscreen_contents');
    }
}
