<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'user_id' => 'master',
            'name' => '마스터',
            'user_type' => 'master',
            'password' => Hash::make('master'),
        ]);
    }
}
