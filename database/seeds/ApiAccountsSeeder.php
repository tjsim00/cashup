<?php

use App\ApiAccounts;
use Illuminate\Database\Seeder;

class ApiAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiAccounts::create([
                'media_id' => 3,
                'media_name' => 'kdisk',
                'advertising_id' => 2,
                'advertising_name' => 'taproad',
                'user_id' => 'tester01',
                'user_point' => '100',
                'cam_id' => '18911',
                'cam_name' => 'Finnq APP [최초구동]',
                'created_at' => '2020-07-15 03:21:27.0',
                'updated_at' => '2020-07-15 03:21:27.0',
            ]);
        ApiAccounts::create([
            'media_id' => 3,
            'media_name' => 'kdisk',
            'advertising_id' => 2,
            'advertising_name' => 'taproad',
            'user_id' => 'tester01',
            'user_point' => '100',
            'cam_id' => '11496',
            'cam_name' => '롯데홈쇼핑 – 롯데 ON (최초오픈)',
            'created_at' => '2020-07-15 03:21:27.0',
            'updated_at' => '2020-07-15 03:21:27.0',
        ]);
    }
}
