@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><h4>광고주 추가</h4></div>
                        <div class="card-body">
                            @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    @foreach($errors->all() as $error)
                                        {{ $error }}<br/>
                                    @endforeach
                                </div>
                            @endif

                            <form method="POST" action="{{ route('aduser.store') }}">
                                @csrf
                                <input type="hidden" name="user_type" value="ad" >
                                <table class="table table-bordered datatable">
                                    <colgroup>
                                        <col width="180px">
                                        <col width="">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th>회사명</th>
                                        <td>
                                            <input class="form-control" name="company_name" type="text" placeholder="회사명를 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>사업자등록번호</th>
                                        <td>
                                            <input class="form-control" name="business_number" type="text" placeholder="사업자등록번호를 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>아이디</th>
                                        <td>
                                            <input class="form-control" name="user_id" type="text" placeholder="아이디를 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>당당자 명</th>
                                        <td>
                                            <input class="form-control" name="name" type="text" placeholder="담당자를 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>이메일</th>
                                        <td>
                                            <input class="form-control" name="email" type="text" placeholder="이메일을 이메일형식으로 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>홈페이지</th>
                                        <td>
                                            <input class="form-control" name="homepage" type="text" placeholder="홈페이지 url을 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>비밀번호</th>
                                        <td>
                                            <input class="form-control" name="password" type="password" placeholder="비밀번호를 6자 이상 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>비밀번호확인</th>
                                        <td>
                                            <input class="form-control" name="password_confirm" type="password" placeholder="비밀번호를 6자 이상 입력하세요." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>전화번호</th>
                                        <td>
                                            <input class="form-control" name="phone" type="text" placeholder="전화번호를 000-0000-0000 형식에 입력하세요."/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>휴대폰 번호</th>
                                        <td>
                                            <input class="form-control" name="mobile_phone" type="text" placeholder="전화번호를 000-0000-0000 형식에 입력하세요."/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>비고</th>
                                        <td>
                                            <textarea class="form-control" name="bigo" rows="9" placeholder=""></textarea>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div style="text-align: center;">
                                    <button class="btn btn-primary" type="submit">저장</button>
                                    <a class="btn btn-warning" href="{{ route('aduser.index') }}">목록</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
