@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 관리자 목록
                            <button class="btn btn-block btn-danger btn-sm" style="float:right; width: 120px;">선택 삭제</button>
                        </div>
                        <div class="card-body">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    @if(is_array(session()->get('success')))
                                        @foreach (session()->get('success') as $message)
                                            {{ $message }}<br/>
                                        @endforeach
                                    @else
                                        {{ session()->get('success') }}
                                    @endif
                                </div>
                            @endif
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <colgroup>
                                    <col width="50px">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>

                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="" id="allCheck"/>
                                    </th>
                                    <th>아이디</th>
                                    <th>이름</th>
                                    <th>전화번호</th>
                                    <th>휴대폰 번호</th>
                                    <th>등록일자</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="" data-user_id="{{ $user->id }}" />
                                        </td>
                                        <td>{{ $user->user_id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ Carbon\Carbon::parse($user->created_at)->format('Y-m-d') }}</td>
                                        <td>
                                            <a href="{{ url('/masteruser/' . $user->id . '/edit') }}" class="btn btn-block btn-primary btn-sm">수정</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('masteruser.destroy', $user->id ) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-block btn-danger btn-sm">삭제</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
