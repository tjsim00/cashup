@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><h4>매체 수정</h4></div>
                        <div class="card-body">
                            {{--                            <div class="alert alert-success" role="alert"></div>--}}
                            <form method="POST" action="{{ route('mdsetup.update', $media_list->id) }}">
                                @csrf
                                @method('PUT')
                                <table class="table table-bordered datatable">
                                    <colgroup>
                                        <col width="180px">
                                        <col width="">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th>매체 이름(af)</th>
                                        <td>
                                            <input class="form-control" name="name" type="text" value="{{ $media_list->name }}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>광고주</th>
                                        <td>
                                            <select class="form-control" id="ad_id" name="ad_id">
                                                <option value="0">광고주를 선택하세요.</option>
                                                @foreach($adUser_lists as $aduser)
                                                    <option value="{{ $aduser->id }}" @if ($media_list->ad_id == $aduser->id) {{ 'selected' }}  @endif  >{{ $aduser->user_id }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>사이트 URL</th>
                                        <td>
                                            <input class="form-control" name="site_url" type="text" value="{{ $media_list->site_url }}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>비율</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                광고주
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-control" name="biyul_ad" type="text" value="{{ $media_list->biyul_ad }}" />
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                매체
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-control" name="biyul_md" type="text" value="{{ $media_list->biyul_md }}" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>노출여부</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="ON" name="exposure" @if ($media_list->exposure == 'ON') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">ON</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="OFF" name="exposure" @if ($media_list->exposure == 'OFF') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">OFF</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">광고 기능 제한</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                Lock Screen :
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="ON" name="adrestriction_lock_screen" @if ($media_list->adrestriction_lock_screen == 'ON') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">ON</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="OFF" name="adrestriction_lock_screen" @if ($media_list->adrestriction_lock_screen == 'OFF') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">OFF</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                Noti :
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="ON" name="adrestriction_noti" @if ($media_list->adrestriction_noti == 'ON') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">ON</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="OFF" name="adrestriction_noti" @if ($media_list->adrestriction_noti == 'OFF') {{ 'checked' }}  @endif >
                                                <label class="form-check-label" for="inline-radio1">OFF</label>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>비고</th>
                                        <td>
                                            <textarea class="form-control" name="bigo" rows="9" placeholder="">{{ $media_list->bigo }}</textarea>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div style="text-align: center;">
                                    <button class="btn btn-primary" type="submit">저장</button>
                                    <a class="btn btn-warning" href="{{ route('masteruser.index') }}">목록</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
