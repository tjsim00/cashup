@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><h4>포인트 설정</h4></div>
                        <div class="card-body">
                            {{--                            <div class="alert alert-success" role="alert"></div>--}}
                            <form method="POST" action="{{ route('media.preferences.update', $mediaPreferences_list[0]['id']) }}">
                                @csrf
                                @method('PUT')
                                <table class="table table-bordered datatable">
                                    <colgroup>
                                        <col width="180px">
                                        <col width="">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th>기능 ON/OFF</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="on" name="status" @if ($mediaPreferences_list[0]['status'] == 'on') {{ 'checked' }}  @endif>
                                                <label class="form-check-label" for="inline-radio1">ON</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="off" name="status"  @if ($mediaPreferences_list[0]['status'] == 'off') {{ 'checked' }}  @endif>
                                                <label class="form-check-label" for="inline-radio1">OFF</label>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>데이터 업데이트 갱신시간</th>
                                        <td>
                                            <input class="form-control" name="updatetime" type="text" value="{{ $mediaPreferences_list[0]['updatetime'] }}" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>1회에 지급되는 기본 포인트</th>
                                        <td>
                                            <input class="form-control" name="freepoint" type="text" value="{{ $mediaPreferences_list[0]['freepoint'] }}" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>1회에 지급되는 총 기본 포인트</th>
                                        <td>
                                            <input class="form-control" name="freelimit" type="text" value="{{ $mediaPreferences_list[0]['freelimit'] }}" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>기본포인트 지급주기 : 3번 락 풀때 1번 지급</th>
                                        <td>
                                            <input class="form-control" name="freequency" type="text" value="{{ $mediaPreferences_list[0]['freequency'] }}" />
                                        </td>
                                    </tr>


                                    </tbody>
                                </table>
                                <div style="text-align: center;">
                                    <button class="btn btn-primary" type="submit">저장</button>
                                    <a class="btn btn-warning" href="{{ route('mdsetup.index') }}">목록</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
