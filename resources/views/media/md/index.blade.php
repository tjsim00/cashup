@extends('dashboard.base')

@section('content')


    <div class="container-fluid">
        <div class="fade-in">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 매체 목록
                            <button class="btn btn-block btn-danger btn-sm" style="float:right; width: 120px;">선택 삭제</button>
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <colgroup>
                                    <col width="50px">
                                    <col width="140px">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>

                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="" id="allCheck"/>
                                    </th>
                                    <th>매체</th>
                                    <th>매체이름(af)</th>
                                    <th>사이트</th>
                                    <th>광고주</th>
                                    <th>비율</th>
                                    <th>노출여부</th>
                                    <th>광고 기능제한(On)</th>
                                    <th>비고</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($media_list as $media)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="" data-user_id="" />
                                        </td>
                                        <td>{{ $media->md_id }}</td>
                                        <td>{{ $media->name }}</td>
                                        <td>{{ $media->site_url }}</td>
                                        <td>{{ $media->ad_id }}</td>
                                        <td>{{ $media->biyul_ad }} : {{ $media->biyul_md }}</td>
                                        <td>{{ $media->exposure }}</td>
                                        <td>{{ $media->adrestriction_lock_screen }} {{ $media->adrestriction_noti }}</td>
                                        <td>{{ $media->bigo }}</td>
                                        <td>
                                            {{--                                            'mdsetup/preferences'--}}
                                            <a href="{{ url('/mdsetup/preferences/' . $media->id) }}" class="btn btn-block btn-flickr btn-sm">포인트 설정</a>
                                        </td>
                                        <td>
                                            <a href="{{ url('/mdsetup/' . $media->id . '/edit') }}" class="btn btn-block btn-primary btn-sm">수정</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('mdsetup.destroy', $media->id ) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-block btn-danger btn-sm">삭제</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{ $media_list->links() }}
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
