@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 시간대별 보고서
                        </div>

                        <div class="card-body" style="overflow: auto">
                            <div class="col-sm-3" style="margin: auto">
                                <form class="form-horizontal" action="" method="GET">
                                    <div class="form-group row">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <input class="form-control" id="reportrange" type="text" name="sch" value="{{ $sch }}" placeholder="" autocomplete="" style="text-align: center"  >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">매체&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <select class="form-control" name="sch_media">
                                                        <option value="" @if ($sch_media == "") {{ 'selected' }}  @endif>전체</option>
                                                        @foreach($media_list as $medias)
                                                            <option value="{{ $medias->name }}" @if ($sch_media == $medias->name) {{ 'selected' }}  @endif  >{{ $medias->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <span class="input-group-append">
                                                <button class="btn btn-facebook" type="submit" style="z-index: 0;width: 100%">검색</button>
                                            </span>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            {{--                            <div id="chartdiv"></div>--}}
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm" style="text-align: center">
                                <colgroup>
                                    <col width="50px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align: middle !important;">Date</th>
                                    <th colspan="2">Taproad</th>
                                    <th colspan="2">Tapjoy</th>
                                    <th colspan="2">Appall</th>
                                </tr>
                                <tr>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($datas as $datass)
                                    <tr>
                                        <td>{{ $datass['date'] }}</td>
                                        <td>{{ number_format($datass['taproad_point']) }}</td>
                                        <td>{{ number_format($datass['taproad_count']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_point']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_count']) }}</td>
                                        <td>{{ number_format($datass['appall_point']) }}</td>
                                        <td>{{ number_format($datass['appall_count']) }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td> 합계 </td>
                                    <td>{{ number_format($total['taproad_point']) }}</td>
                                    <td>{{ number_format($total['taproad_count']) }}</td>
                                    <td>{{ number_format($total['tapjoy_point']) }}</td>
                                    <td>{{ number_format($total['tapjoy_count']) }}</td>
                                    <td>{{ number_format($total['appall_point']) }}</td>
                                    <td>{{ number_format($total['appall_count']) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }
    </style>
@endsection

@section('javascript')

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(function() {
            //input을 datepicker로 선언
            $("#reportrange").datepicker({
                dateFormat: 'yy-mm-dd' //Input Display Format 변경
                ,showOtherMonths: true //빈 공간에 현재월의 앞뒤월의 날짜를 표시
                ,showMonthAfterYear:true //년도 먼저 나오고, 뒤에 월 표시
                // ,changeYear: true //콤보박스에서 년 선택 가능
                // ,changeMonth: true //콤보박스에서 월 선택 가능
                // ,showOn: "both" //button:버튼을 표시하고,버튼을 눌러야만 달력 표시 ^ both:버튼을 표시하고,버튼을 누르거나 input을 클릭하면 달력 표시
                // ,buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif" //버튼 이미지 경로
                // ,buttonImageOnly: true //기본 버튼의 회색 부분을 없애고, 이미지만 보이게 함
                // ,buttonText: "선택" //버튼에 마우스 갖다 댔을 때 표시되는 텍스트
                ,yearSuffix: "년" //달력의 년도 부분 뒤에 붙는 텍스트
                ,monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'] //달력의 월 부분 텍스트
                ,monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 Tooltip 텍스트
                ,dayNamesMin: ['일','월','화','수','목','금','토'] //달력의 요일 부분 텍스트
                ,dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'] //달력의 요일 부분 Tooltip 텍스트
                // ,minDate: "-1M" //최소 선택일자(-1D:하루전, -1M:한달전, -1Y:일년전)
                // ,maxDate: "+1M" //최대 선택일자(+1D:하루후, -1M:한달후, -1Y:일년후)
            });

            //초기값을 오늘 날짜로 설정
            $('#reportrange').datepicker('setDate', '{{ $sch }}'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
        });
    </script>
@endsection
