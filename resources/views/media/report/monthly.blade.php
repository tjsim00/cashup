@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 월별 보고서
                        </div>
                        <div class="card-body" style="overflow: auto">
                            <div class="col-sm-3" style="margin: auto">
                                <form class="form-horizontal" action="" method="GET">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <select class="form-control" name="sch">
                                                        <option value="2018" @if ($sch == "2018") {{ 'selected' }}  @endif>2018</option>
                                                        <option value="2019" @if ($sch == "2019") {{ 'selected' }}  @endif>2019</option>
                                                        <option value="2020" @if ($sch == "2020") {{ 'selected' }}  @endif>2020</option>
                                                        <option value="2021" @if ($sch == "2021") {{ 'selected' }}  @endif>2021</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">매체&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    </div>
                                                    <select class="form-control" name="sch_media">
                                                        <option value="" @if ($sch_media == "") {{ 'selected' }}  @endif>전체</option>
                                                        @foreach($media_list as $medias)
                                                            <option value="{{ $medias->name }}" @if ($sch_media == $medias->name) {{ 'selected' }}  @endif  >{{ $medias->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <span class="input-group-append">
                                                <button class="btn btn-facebook" type="submit" style="z-index: 0;width: 100%">검색</button>
                                            </span>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            {{--                            <div id="chartdiv"></div>--}}
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm" style="text-align: center">
                                <colgroup>
                                    <col width="50px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align: middle !important;">Date</th>
                                    <th colspan="2">Taproad</th>
                                    <th colspan="2">Tapjoy</th>
                                    <th colspan="2">Appall</th>
                                </tr>
                                <tr>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($datas as $datass)
                                    <tr>
                                        <td>{{ $datass['date'] }}</td>
                                        <td>{{ number_format($datass['taproad_point']) }}</td>
                                        <td>{{ number_format($datass['taproad_count']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_point']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_count']) }}</td>
                                        <td>{{ number_format($datass['appall_point']) }}</td>
                                        <td>{{ number_format($datass['appall_count']) }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td> 합계 </td>
                                    <td>{{ number_format($total['taproad_point']) }}</td>
                                    <td>{{ number_format($total['taproad_count']) }}</td>
                                    <td>{{ number_format($total['tapjoy_point']) }}</td>
                                    <td>{{ number_format($total['tapjoy_count']) }}</td>
                                    <td>{{ number_format($total['appall_point']) }}</td>
                                    <td>{{ number_format($total['appall_count']) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }
    </style>
@endsection

@section('javascript')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $(function(){

        })
    </script>
@endsection
