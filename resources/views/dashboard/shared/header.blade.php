<div class="c-wrapper">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
            <span class="c-header-toggler-icon"></span>
        </button>
        <a class="c-header-brand d-sm-none" href="#">
            <img class="c-header-brand" src="/assets/brand/coreui-base.svg" width="97" height="46" alt="CoreUI Logo">
        </a>
        <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
            <span class="c-header-toggler-icon"></span>
        </button>

        @php ($header =  'master')

        @if (class_basename(Route::current()->controller) === "masterUserController"
            || class_basename(Route::current()->controller) === "adUserController"
            || class_basename(Route::current()->controller) === "mdUserController"
             )
            @php ($header =  'master')

        @elseif (class_basename(Route::current()->controller) === "adveriserMainContorller"
            || class_basename(Route::current()->controller) === "adController"
            || class_basename(Route::current()->controller) === "adreportController"
            || class_basename(Route::current()->controller) === "adlogController"
            )

            @php ($header =  'ad')
        @elseif (class_basename(Route::current()->controller) === "mediaMainContorller"
            || class_basename(Route::current()->controller) === "mdController"
            || class_basename(Route::current()->controller) === "adreportController"
            || class_basename(Route::current()->controller) === "adlogController"
            )

            @php ($header =  'md')
        @endif
        <ul class="c-header-nav d-md-down-none">
            @if (Config::get('user_type') == "master")
            <li class="c-header-nav-item  px-3 ">
                <a class="c-header-nav-link @if ($header == 'master') {{ 'c-active' }}  @endif  " role="button" aria-expanded="false" href="{{ route('masteruser.index') }}">환경설정</a>
            </li>
            @endif
            @if (Config::get('user_type') == "master" || Config::get('user_type') == "ad")
            <li class="c-header-nav-item  px-3">
                <a class="c-header-nav-link  @if ($header == 'ad') {{ 'c-active' }}  @endif " role="button" aria-expanded="false" href="{{ route('adsetup.index') }}">광고 관리</a>
            </li>
            @endif
            @if (Config::get('user_type') == "master" || Config::get('user_type') == "md")
            <li class="c-header-nav-item  px-3">
                @if (Config::get('user_type') == "master")
                    <a class="c-header-nav-link @if ($header == 'md') {{ 'c-active' }}  @endif " role="button" aria-expanded="false" href="{{ route('mdsetup.index') }}">매체 관리</a>
                @elseif (Config::get('user_type') == "md")
                    <a class="c-header-nav-link @if ($header == 'md') {{ 'c-active' }}  @endif " role="button" aria-expanded="false" href="{{ route('md.monthly') }}">매체 관리</a>
                @endif
            </li>
            @endif
        </ul>

        <ul class="c-header-nav ml-auto mr-4">
{{--            <li>  관리자님 안녕하세요 / today: {{ date("Y.m.d") }}</li>--}}
{{--            <li>  {{ Auth::guard('md')->user()->user_id }} 관리자님 안녕하세요 / today: {{ date("Y.m.d") }}</li>--}}
        </ul>

        <ul class="c-header-nav ml-auto mr-4">
            <li class="c-header-nav-item dropdown">
                <a class="dropdown-item" href="#">
                    <svg class="c-icon mr-2">
                        <use xlink:href="/icons/sprites/free.svg#cil-account-logout"></use>
                    </svg>
                    @if (Config::get('user_type') == "master")
                        <form action="{{ route('master_logout') }}" method="POST"> @csrf <button type="submit" class="btn btn-ghost-dark btn-block">Logout</button></form>
                    @elseif (Config::get('user_type') == "md")
                        <form action="{{ route('md_logout') }}" method="POST"> @csrf <button type="submit" class="btn btn-ghost-dark btn-block">Logout</button></form>
                    @elseif (Config::get('user_type') == "ad")
                        <form action="{{ route('ad_logout') }}" method="POST"> @csrf <button type="submit" class="btn btn-ghost-dark btn-block">Logout</button></form>
                    @endif
                </a>
            </li>
        </ul>
    </header>
