
<div class="c-sidebar-brand">
    {{--    <img class="c-sidebar-brand-full" src="/assets/brand/coreui-base-white.svg" width="118" height="46" alt="CoreUI Logo">--}}
    {{--    <img class="c-sidebar-brand-minimized" src="assets/brand/coreui-signet-white.svg" width="118" height="46" alt="CoreUI Logo">--}}
</div>

{{--style="background-color: #321fdb"--}}

<ul class="c-sidebar-nav">


@if (class_basename(Route::current()->controller) === "masterUserController"
    || class_basename(Route::current()->controller) === "adUserController"
    || class_basename(Route::current()->controller) === "mdUserController"
     )
    <!-- 환경 설정 -->
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                관리자
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "masteruser.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('masteruser.index') }}"><span class="c-sidebar-nav-icon"></span>관리자 목록</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "masteruser.create" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('masteruser.create') }}"><span class="c-sidebar-nav-icon"></span>관리자 추가</a></li>
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                광고주
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "aduser.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('aduser.index') }}"><span class="c-sidebar-nav-icon"></span>광고주 목록</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "aduser.create" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('aduser.create') }}"><span class="c-sidebar-nav-icon"></span>광고주 추가</a></li>
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                매체사
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "mduser.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('mduser.index') }}"><span class="c-sidebar-nav-icon"></span>매체사 목록</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "mduser.create" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('mduser.create') }}"><span class="c-sidebar-nav-icon"></span>매체사 추가</a></li>
            </ul>
        </li>
    @elseif (class_basename(Route::current()->controller) === "adveriserMainContorller"
        || class_basename(Route::current()->controller) === "adController"
        || class_basename(Route::current()->controller) === "adreportController"
        || class_basename(Route::current()->controller) === "adlogController"
        )

        @if (Config::get('user_type') == "master")
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                매체 선택
            </a>

            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item">
                <span class="c-sidebar-nav-link c-active" style="padding-left:0px;">
                        <select class="form-control" id="adUserSelect" name="adUserSelect" style="width: 180px; margin: 0 auto; ">
                          <option value="">광고주를 선택하세요.</option>
                          @foreach($adUser_lists as $aduser)
                                <option value="{{ $aduser->id }}" @if (Session::get('adUserss_id') == $aduser->id) {{ 'selected' }}  @endif  >{{ $aduser->user_id }}</option>
                          @endforeach
                        </select>
                </span>
                </li>
            </ul>
        </li>
        @endif
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                광고 관리
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "adsetup.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('adsetup.index') }}"><span class="c-sidebar-nav-icon"></span>광고 목록</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "adsetup.create" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('adsetup.create') }}"><span class="c-sidebar-nav-icon"></span>광고 등록</a></li>
            </ul>
        </li>
        @if (Config::get('user_type') == "master")
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                보고서
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.monthly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.monthly') }}"><span class="c-sidebar-nav-icon"></span>월별</a></li>
{{--                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.donthly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.donthly') }}"><span class="c-sidebar-nav-icon"></span>일별</a></li>--}}
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.hourly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.hourly') }}"><span class="c-sidebar-nav-icon"></span>시간대 별</a></li>
{{--                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.bymedia" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.bymedia') }}"><span class="c-sidebar-nav-icon"></span>매체 별</a></li>--}}
{{--                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.byadvertiser" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.byadvertiser') }}"><span class="c-sidebar-nav-icon"></span>광고 별</a></li>--}}
{{--                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "ad.byperiod" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('ad.byperiod') }}"><span class="c-sidebar-nav-icon"></span>기간 별</a></li>--}}
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                사용자 LOG
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "adlog.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('adlog.index') }}"><span class="c-sidebar-nav-icon"></span>사용자 LOG</a></li>
            </ul>
        </li>
        @endif
    @elseif (class_basename(Route::current()->controller) === "mediaMainContorller"
        || class_basename(Route::current()->controller) === "mdController"
        || class_basename(Route::current()->controller) === "mdreportController"
        || class_basename(Route::current()->controller) === "mdlogController"
        )

        @if (Config::get('user_type') == "master")
            <li class="c-sidebar-nav-dropdown c-show">
                <a class="c-sidebar-nav-link">
                    <i class="cil-speedometer c-sidebar-nav-icon"></i>
                    매체 선택
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                    <span class="c-sidebar-nav-link c-active" style="padding-left:0px;">
                            <select class="form-control" id="mdUserSelect" name="mdUserSelect" style="width: 180px; margin: 0 auto; ">
                              <option value="">매체를 선택하세요.</option>
                              @foreach($mdUser_lists as $mduser)
                                    <option value="{{ $mduser->id }}" @if (Session::get('mdUserss_id') == $mduser->id) {{ 'selected' }}  @endif  >{{ $mduser->user_id }}</option>
                                @endforeach
                            </select>
                    </span>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-dropdown c-show">
                <a class="c-sidebar-nav-link">
                    <i class="cil-speedometer c-sidebar-nav-icon"></i>
                    매체 관리
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "mdsetup.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('mdsetup.index') }}"><span class="c-sidebar-nav-icon"></span>매체 목록</a></li>
                    <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "mdsetup.create" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('mdsetup.create') }}"><span class="c-sidebar-nav-icon"></span>매체 등록</a></li>
                </ul>
            </li>
        @endif
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                보고서
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "md.monthly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('md.monthly') }}"><span class="c-sidebar-nav-icon"></span>월별</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "md.donthly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('md.donthly') }}"><span class="c-sidebar-nav-icon"></span>일별</a></li>
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "md.hourly" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('md.hourly') }}"><span class="c-sidebar-nav-icon"></span>시간대 별</a></li>
{{--                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "md.byadvertiser" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('md.byadvertiser') }}"><span class="c-sidebar-nav-icon"></span>광고 별</a></li>--}}
            </ul>
        </li>
        <li class="c-sidebar-nav-dropdown c-show">
            <a class="c-sidebar-nav-link">
                <i class="cil-speedometer c-sidebar-nav-icon"></i>
                사용자 LOG
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item" @if (Route::currentRouteName() == "mdlog.index" ) style="background-color: #321fdb" @endif ><a class="c-sidebar-nav-link c-active" href="{{ route('mdlog.index') }}"><span class="c-sidebar-nav-icon"></span>사용자 LOG</a></li>
            </ul>
        </li>
    @endif

</ul>

@section('javascript')
    <script>
        $(function(){
            @if (Config::get('user_type') == "master")
                $("#adUserSelect").change(function(){
                    var data = new Object();
                    data.adUserid = $(this).val();
                    var jsonData = JSON.stringify(data);
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{ route('AdUser.change') }}",
                        method: "POST",
                        dataType: "json",
                        data: {'data': jsonData},
                        success: function (data) {
                            var JSONArray = JSON.parse(JSON.stringify(data));

                            if (JSONArray['result'] == "success") {
                                window.location.href = '{{ route('adsetup.index') }}';
                            } else if (JSONArray['result'] == "error") {
                                alert(JSONArray['error_message']);
                            };
                        },
                        error: function () {
                            alert("Error while getting results");
                        }
                    });
                });
                $("#mdUserSelect").change(function(){
                    var data = new Object();
                    data.mdUserid = $(this).val();
                    var jsonData = JSON.stringify(data);
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{ route('MdUser.change') }}",
                        method: "POST",
                        dataType: "json",
                        data: {'data': jsonData},
                        success: function (data) {
                            var JSONArray = JSON.parse(JSON.stringify(data));

                            if (JSONArray['result'] == "success") {
                                window.location.href = '{{ route('mdsetup.index') }}';
                            } else if (JSONArray['result'] == "error") {
                                alert(JSONArray['error_message']);
                            };
                        },
                        error: function () {
                            alert("Error while getting results");
                        }
                    });
                });
            @elseif (Config::get('user_type') == "md")
                $("#mdUserSelect").change(function(){
                var data = new Object();
                data.mdUserid = $(this).val();
                var jsonData = JSON.stringify(data);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('MdUser.change') }}",
                    method: "POST",
                    dataType: "json",
                    data: {'data': jsonData},
                    success: function (data) {
                        var JSONArray = JSON.parse(JSON.stringify(data));

                        if (JSONArray['result'] == "success") {
                            window.location.href = '{{ route('mdsetup.index') }}';
                        } else if (JSONArray['result'] == "error") {
                            alert(JSONArray['error_message']);
                        };
                    },
                    error: function () {
                        alert("Error while getting results");
                    }
                });
            });
            @elseif (Config::get('user_type') == "ad")
                $("#adUserSelect").change(function(){
                var data = new Object();
                data.adUserid = $(this).val();
                var jsonData = JSON.stringify(data);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('AdUser.change') }}",
                    method: "POST",
                    dataType: "json",
                    data: {'data': jsonData},
                    success: function (data) {
                        var JSONArray = JSON.parse(JSON.stringify(data));

                        if (JSONArray['result'] == "success") {
                            window.location.href = '{{ route('adsetup.index') }}';
                        } else if (JSONArray['result'] == "error") {
                            alert(JSONArray['error_message']);
                        };
                    },
                    error: function () {
                        alert("Error while getting results");
                    }
                });
            });
            @endif
        })
    </script>

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>
