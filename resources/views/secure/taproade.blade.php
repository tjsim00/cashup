<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style>
        table{font-size:12px;}
        input{font-size:12px;padding:0px;width:100%;border:0px}
        td {border: 1px solid #ccc; padding:5px;}
    </style>
</head>
<!--    -->
<body>
<div id="app">
    <main class="py-4">
        <center>
            <form id="frm" method="post" action="http://cashup.kr/api/taproad/callback" >
                @csrf

                <table width="700" style="margin-top:5%;">
                    <tr>
                        <td width="200">id (매체 회원의 아이디 혹은 고유 값)</td>
                        <td><input type="text" id="id" name="id" value="" placeholder=""></td>
                    </tr>

                    <tr>
                        <td width="200">sec_k (보안 키 값)</td>
                        <td><input type="text" id="sec_k" name="sec_k" value="" placeholder=""></td>
                    </tr>
                    <tr>
                        <td width="200">ad_idx (탭로드 내 해당 광고 번호)</td>
                        <td><input type="text" id="ad_idx" name="ad_idx" value="" placeholder=""></td>
                    </tr>
                    <tr>
                        <td width="200">reward_quantity (리워드 액수)</td>
                        <td><input type="text" id="reward_quantity" name="reward_quantity" value="" placeholder=""></td>
                    </tr>

                    <tr>
                        <td width="200">reward_name (해당 광고 명칭)</td>
                        <td><input type="text" id="reward_name" name="reward_name" value="" placeholder=""></td>
                    </tr>

                    <tr>
                        <td width="200">AppKey (Affiliate별 임의의 키 값)</td>
                        <td><input type="text" id="AppKey" name="AppKey" value="" placeholder=""></td>
                    </tr>

                    <tr>
                        <td width="200">응답 결과</td>
                        <td><input type="submit" style="background-color:#fff;border:1px solid #ccc;" value="전 송" ></td>
                    </tr>
                </table>
            </form>
        </center>
    </main>
</div>
</body>
</html>
