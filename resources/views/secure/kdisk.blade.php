<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style>
        table{font-size:12px;}
        input{font-size:12px;padding:0px;width:100%;border:0px}
        td {border: 1px solid #ccc; padding:5px;}
    </style>
</head>
<body>
<div id="app">

    <main class="py-4">
        <center>
            <form id="frm" method="post" action="{{ url('/') }}">
                @csrf
                <input type="hidden" name="search_flag" value="y">
                <table width="700" style="margin-top:5%;">
                    <tr>
                        <td width="200">적립금 지급 URL</td>
                        <td><input type="text" id="url" name="url" value="{{ $request->url ?? '' }}" placeholder="http://" ></td>
                    </tr>
                    <tr>
                        <td width="200">통신 메소드</td>
                        <td>
                            <select name="method_flag">
                                <option value="GET" >GET</option>
                                <option value="POST" >POST</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="200">매체 코드</td>
                        <td><input type="text" id="inv_id" name="inv_id" value="{{ $request->inv_id ?? 'kdisk' }}" placeholder="사전에 발급받은 매체 코드"></td>
                    </tr>
                    <tr>
                        <td width="200">회원 아이디</td>
                        <td><input type="text" id="inv_hash" name="inv_hash" value="{{ $request->inv_hash ?? '' }}" placeholder="연동 문서에서 inv_hash에 해당하는 데이터"></td>
                    </tr>
                    <tr>
                        <td width="200">지급할 포인트</td>
                        <td><input type="text" id="pointval" name="pointval" value="{{ $request->pointval ?? '' }}" placeholder="100(숫자만 입력)"></td>
                    </tr>
                    <tr>
                        <td width="200">응답 결과</td>
                        <td><input type="submit" style="background-color:#fff;border:1px solid #ccc;" value="전 송"></td>
                    </tr>
                </table>
            </form>

            @if (isset($request))
            <table width="700">
                <tr>
                    <td width="200">접근 메소드</td>
                    <td>{{ $request->method_flag ?? '' }}</td>
                </tr>
                <tr>
                    <td width="200">접근 경로</td>
                    <td>{{ $request->url ?? '' }}</td>
                </tr>
                <tr>
                    <td width="200">접근 파라미터</td>
                    <td style="word-break:break-all;">{{ $output ?? '' }}</td>
                </tr>
                <tr>
                    <td width="200">파라미터 설명</td>
                    <td style="word-break:break-all;">
                        cam_id => 광고코드<br>
                        inv_id => 매체코드<br>
                        inv_hash => 회원아이디<br>
                        uniqcode => 거래번호 (트랜잭션 번호)<br>
                        cam_nm => 캠페인명<br>
                        pointval => 지급할 포인트
                    </td>
                </tr>
                <tr>
                    <td width="200">HTTP 응답</td>
                    <td>{{ $json_data->status ?? '' }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center">응답 결과</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
            </table>
            @endif
        </center>
    </main>
</div>
</body>
</html>
