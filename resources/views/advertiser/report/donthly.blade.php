@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 일별 보고서
                        </div>
                        <div class="card-body" style="overflow: auto">

                            <div class="col-sm-3" style="margin: auto">
                                <form class="form-horizontal" action="" method="GET">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input class="form-control" id="reportrange" type="text" name="sch" value="" placeholder="" autocomplete="" style="text-align: center"  >
                                                <span class="input-group-append">
                                                    <button class="btn btn-facebook" type="submit" style="z-index: 0;">검색</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
{{--                            <div id="chartdiv"></div>--}}
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm" style="text-align: center">
                                <colgroup>
                                    <col width="50px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th rowspan="2" style="vertical-align: middle !important;">Date</th>
                                    <th colspan="2">Taproad</th>
                                    <th colspan="2">Tapjoy</th>
                                </tr>
                                <tr>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                    <th>포인트</th>
                                    <th>참여수</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($datas as $datass)
                                    <tr>
                                        <td>{{ $datass['date'] }}</td>
                                        <td>{{ number_format($datass['taproad_point']) }}</td>
                                        <td>{{ number_format($datass['taproad_count']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_point']) }}</td>
                                        <td>{{ number_format($datass['tapjoy_count']) }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td> 합계 </td>
                                    <td>{{ number_format($total['taproad_point']) }}</td>
                                    <td>{{ number_format($total['taproad_count']) }}</td>
                                    <td>{{ number_format($total['tapjoy_point']) }}</td>
                                    <td>{{ number_format($total['tapjoy_count']) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        #chartdiv {
            width: {{ $chart_width }}%;
            height: 600px;
        }

    </style>
@endsection

@section('javascript')

    <script>
        $(function() {


        });

    </script>
@endsection
