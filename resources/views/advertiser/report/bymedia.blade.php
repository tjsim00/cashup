@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">

                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
