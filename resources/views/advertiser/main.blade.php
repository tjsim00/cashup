@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> 광고 목록
                            <button class="btn btn-block btn-danger btn-sm" style="float:right; width: 120px;">선택 삭제</button>
                        </div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <colgroup>
                                    <col width="50px">
                                    <col width="140px">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="">
                                    <col width="120px">
                                    <col width="120px">
                                </colgroup>

                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="" id="allCheck"/>
                                    </th>
                                    <th>아이콘</th>
                                    <th>제목</th>
                                    <th>내용</th>
                                    <th>구분</th>
                                    <th>가격</th>
                                    <th>상태</th>
                                    <th>기간</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($advertising_list as $advertising)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="" data-user_id="" />
                                        </td>
                                        <td>
                                            @if($advertising->thum_image && file_exists(public_path().'/storage/advertising/'.$advertising->id.'/thumbnail/'.$advertising->thum_image))
                                                <img src="{{'/advertising/'.$advertising->id.'/thumbnail/'.$advertising->thum_image}}">
                                            @endif
                                        </td>
                                        <td>{{ $advertising->title }}</td>
                                        <td>{{ $advertising->content }}</td>
                                        <td>{{ $advertising->division }}</td>
                                        <td>{{ $advertising->price }}</td>
                                        <td>{{ $advertising->stutus }}</td>
                                        <td>{{ $advertising->period }}</td>
                                        <td>
                                            <a href="{{ url('/adsetup/' . $advertising->id . '/edit') }}" class="btn btn-block btn-primary btn-sm">수정</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('adsetup.destroy', $advertising->id ) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-block btn-danger btn-sm">삭제</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{ $advertising_list->links() }}
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
