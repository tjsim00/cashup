@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><h4>광고 추가</h4></div>
                        <div class="card-body">
                            {{--                            <div class="alert alert-success" role="alert"></div>--}}
                            <form method="POST" action="{{ route('adsetup.store') }}" enctype="multipart/form-data">
                                @csrf
                                <table class="table table-bordered datatable">
                                    <colgroup>
                                        <col width="180px">
                                        <col width="">
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th>광고 설정</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="LockScreen" name="setting">
                                                <label class="form-check-label" for="inline-radio1">Lock Screen</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="Noti" name="setting">
                                                <label class="form-check-label" for="inline-radio1">Noti</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>광고 형식 (ad_type) </th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="offerwall" name="formality">
                                                <label class="form-check-label" for="inline-radio1">offerwall</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="web" name="formality">
                                                <label class="form-check-label" for="inline-radio1">web</label>
                                            </div>

                                            {{--                                            <div class="form-check form-check-inline mr-1">--}}
                                            {{--                                                <input class="form-check-input" id="inline-radio1" type="radio" value="list" name="formality">--}}
                                            {{--                                                <label class="form-check-label" for="inline-radio1">리스트 형</label>--}}
                                            {{--                                            </div>--}}
                                            {{--                                            <div class="form-check form-check-inline mr-1">--}}
                                            {{--                                                <input class="form-check-input" id="inline-radio1" type="radio" value="image" name="formality">--}}
                                            {{--                                                <label class="form-check-label" for="inline-radio1">이미지 형</label>--}}
                                            {{--                                            </div>--}}
                                            {{--                                            <div class="form-check form-check-inline mr-1">--}}
                                            {{--                                                <input class="form-check-input" id="inline-radio1" type="radio" value="image+text" name="formality">--}}
                                            {{--                                                <label class="form-check-label" for="inline-radio1">이미지 + 텍스트</label>--}}
                                            {{--                                            </div>--}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>이미지 등록</th>
                                        <td>
                                            <input type="file" name="ori_image" id="ori_image">
                                        </td>
                                    </tr>
                                    <!--
                                    <tr>
                                        <th>상세 이미지 등록</th>
                                        <td>
                                            <input class="form-control" name="passwod" type="text"/>
                                        </td>
                                    </tr>
                                    -->
                                    <tr>
                                        <th>제목 ( ad_id )</th>
                                        <td>
                                            <input class="form-control" name="title" type="text"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>내용</th>
                                        <td>
                                            <textarea class="form-control" name="content" rows="9" placeholder=""></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>구분</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="chamyeo" name="division">
                                                <label class="form-check-label" for="inline-radio1">참여</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="gaib" name="division">
                                                <label class="form-check-label" for="inline-radio1">가입</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="silhaeng" name="division">
                                                <label class="form-check-label" for="inline-radio1">실행</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="seolchi" name="division">
                                                <label class="form-check-label" for="inline-radio1">설치</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="silhaeng+seolchi" name="division">
                                                <label class="form-check-label" for="inline-radio1">설치+실행</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>가격</th>
                                        <td>
                                            <input class="form-control" name="price" type="text"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>광고 URL ( ad_url )</th>
                                        <td>
                                            <input class="form-control" name="url" type="text"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>상태</th>
                                        <td>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="ing" name="stutus">
                                                <label class="form-check-label" for="inline-radio1">광고중</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="stop" name="stutus">
                                                <label class="form-check-label" for="inline-radio1">광고중</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>기간</th>
                                        <td>
                                            {{--                                            <input class="form-control" name="period" type="text"/>--}}
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="year" name="period">
                                                <label class="form-check-label" for="inline-radio1">1년 365일</label>
                                            </div>
                                            <div class="form-check form-check-inline mr-1">
                                                <input class="form-check-input" id="inline-radio1" type="radio" value="seoljeong" name="period">
                                                <label class="form-check-label" for="inline-radio1">기간 설정</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>제한 설정</th>
                                        <td>
                                            <input class="form-control" name="restriction" type="text"/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div style="text-align: center;">
                                    <button class="btn btn-primary" type="submit">저장</button>
                                    <a class="btn btn-warning" href="{{ route('adsetup.index') }}">목록</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('javascript')
    <script>
        $(function(){

            $("#allCheck").click(function(){

                if($("#allCheck").prop("checked")) {

                    $("input[type=checkbox]").prop("checked",true);

                } else {
                    $("input[type=checkbox]").prop("checked",false);
                }
            })
        })
    </script>
@endsection
