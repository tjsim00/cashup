<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::domain('md.' . env('SITE_URL'))->group(function() {
    Auth::routes();

    Route::get('/', function () {
        return view('coreui');
    })->middleware('Auth.md');


    Route::get('/register/md', 'Auth\RegisterController@showMdRegisterForm')->name('md_register');
    Route::post('/register/md', 'Auth\RegisterController@createMd')->name('md_register');

    Route::get('/login/md', 'Auth\LoginController@showMdLoginForm')->name('md_login');
    Route::post('/login/md', 'Auth\LoginController@mdLogin')->name('md_login');

    Route::post('/logout/md', 'Auth\LogoutController@mdLogout')->name('md_logout');

    Route::get('/home', 'HomeController@index')->name('home');

    /* 매체사 관리 */
    // 메인
    Route::get('/media/main','media\mediaMainContorller@index')->name('media.main');
    Route::Post('/ajaxMdUserChange', 'media\mediaMainContorller@mdUserChange')->name('MdUser.change');
    // 매체관리
    Route::resource('mdsetup', 'media\mdController')->middleware('mdUser.Auth')->except('show');
    Route::get('mdsetup/preferences/{mdsetup}', 'media\mdController@preferences')->middleware('mdUser.Auth')->name('media.preferences');
    Route::put('mdsetup/preferences/{mdsetup}', 'media\mdController@preferences_update')->middleware('mdUser.Auth')->name('media.preferences.update');
    // 보고서
//    Route::resource('mdreport', 'media\mdreportController');
    Route::get('/mdreport/monthly', 'media\mdreportController@Monthly')->name('md.monthly');
    Route::get('/mdreport/donthly', 'media\mdreportController@Daily')->name('md.donthly');
    Route::get('/mdreport/hourly', 'media\mdreportController@Hourly')->name('md.hourly');
    Route::get('/mdreport/byadvertiser', 'media\mdreportController@ByAdvertiser')->name('md.byadvertiser');
    // log
    Route::resource('mdlog', 'media\mdlogController');

});
