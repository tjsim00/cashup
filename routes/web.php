<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/taproade', function () {
    return view('secure.taproade');
});

Route::domain('secure.' . env('SITE_URL'))->group(function() {
    Route::get('/', function () {
        return view('secure.kdisk');
    });

    Route::post('/','secure\kdiskController@test');
});

Route::domain( env('SITE_URL'))->group(function() {
    Route::get('/', function () {
        if (!Config::get('user_type')) {
            abort(403, "Please check the route again.");
            exit;
        }
    });
});
