<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::domain('master.' . env('SITE_URL'))->group(function() {
    Auth::routes();

//    Route::get('/', function () {
//        return view('coreui');
//    })->middleware('Auth.master');

    Route::get('/', 'MainController@index')->middleware('Auth.master');

    // 테스트 page
    Route::resource('topics', 'TopicController');
//    Route::get('/home', 'HomeController@index')->name('home');

    /* 환경설정 */
    // 관리자
    Route::resource('masteruser', 'configuration\masterUserController')->except('show');
    // 광고주
    Route::resource('aduser', 'configuration\adUserController')->except('show');
    // 매체자
    Route::resource('mduser', 'configuration\mdUserController')->except('show');


    /* 광고주관리 */
    // 메인
    Route::get('/adveriser/main','advertiser\adveriserMainContorller@index')->name('adveriser.main');
    Route::Post('/ajaxAdUserChange', 'advertiser\adveriserMainContorller@adUserChange')->name('AdUser.change');
    // 광고관리
    Route::resource('adsetup', 'advertiser\adController')->middleware('adUser.Auth')->except('show');;
    // 보고서
//    Route::resource('adreport', 'advertiser\adreportController');
    Route::get('/adreport/monthly', 'advertiser\adreportController@Monthly')->name('ad.monthly');
    Route::get('/adreport/donthly', 'advertiser\adreportController@Daily')->name('ad.donthly');
    Route::get('/adreport/hourly', 'advertiser\adreportController@Hourly')->name('ad.hourly');
    Route::get('/adreport/bymedia', 'advertiser\adreportController@ByMedia')->name('ad.bymedia');
    Route::get('/adreport/byadvertiser', 'advertiser\adreportController@ByAdvertiser')->name('ad.byadvertiser');
    Route::get('/adreport/byperiod', 'advertiser\adreportController@ByPeriod')->name('ad.byperiod');
    // log
    Route::resource('adlog', 'advertiser\adlogController');

    /* 매체사 관리 */
    // 메인
    Route::get('/media/main','media\mediaMainContorller@index')->name('media.main');
    Route::Post('/ajaxMdUserChange', 'media\mediaMainContorller@mdUserChange')->name('MdUser.change');
    // 매체관리
    Route::resource('mdsetup', 'media\mdController')->middleware('mdUser.Auth')->except('show');
    Route::get('mdsetup/preferences/{mdsetup}', 'media\mdController@preferences')->middleware('mdUser.Auth')->name('media.preferences');
    Route::put('mdsetup/preferences/{mdsetup}', 'media\mdController@preferences_update')->middleware('mdUser.Auth')->name('media.preferences.update');
    // 보고서
//    Route::resource('mdreport', 'media\mdreportController');
    Route::get('/mdreport/monthly', 'media\mdreportController@Monthly')->name('md.monthly');
    Route::get('/mdreport/donthly', 'media\mdreportController@Daily')->name('md.donthly');
    Route::get('/mdreport/hourly', 'media\mdreportController@Hourly')->name('md.hourly');
    Route::get('/mdreport/byadvertiser', 'media\mdreportController@ByAdvertiser')->name('md.byadvertiser');
    // log
    Route::resource('mdlog', 'media\mdlogController');


    // 로그인
    Route::get('/login/master', 'Auth\LoginController@showMasterLoginForm')->name('master_login');
    Route::post('/login/master', 'Auth\LoginController@masterLogin')->name('master_login');
    // 로그아웃
    Route::post('/logout/master', 'Auth\LogoutController@masterLogout')->name('master_logout');
    // 회원 가입
    Route::get('/register/master', 'Auth\RegisterController@showMasterRegisterForm')->name('master_register');
    Route::post('/register/master', 'Auth\RegisterController@createMaster')->name('master_register');

});
