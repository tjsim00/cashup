<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::domain('ad.' . env('SITE_URL'))->group(function() {
    Auth::routes();

    Route::get('/', function () {
        return view('coreui');
    })->middleware('Auth.ad');


    Route::get('/register/ad', 'Auth\RegisterController@showAdRegisterForm')->name('ad_register');
    Route::post('/register/ad', 'Auth\RegisterController@createAd')->name('ad_register');

    Route::get('/login/ad', 'Auth\LoginController@showAdLoginForm')->name('ad_login');
    Route::post('/login/ad', 'Auth\LoginController@adLogin')->name('ad_login');

    Route::post('/logout/ad', 'Auth\LogoutController@adLogout')->name('ad_logout');

    Route::get('/home', 'HomeController@index')->name('home');

    /* 광고주관리 */
    // 메인
    Route::get('/adveriser/main','advertiser\adveriserMainContorller@index')->name('adveriser.main');
    Route::Post('/ajaxAdUserChange', 'advertiser\adveriserMainContorller@adUserChange')->name('AdUser.change');

    // 광고관리
    Route::resource('adsetup', 'advertiser\adController')->middleware('adUser.Auth')->except('show');
    // 보고서
//    Route::resource('adreport', 'advertiser\adreportController');
    Route::get('/adreport/monthly', 'advertiser\adreportController@Monthly')->name('ad.monthly');
    Route::get('/adreport/donthly', 'advertiser\adreportController@Daily')->name('ad.donthly');
    Route::get('/adreport/hourly', 'advertiser\adreportController@Hourly')->name('ad.hourly');
    Route::get('/adreport/bymedia', 'advertiser\adreportController@ByMedia')->name('ad.bymedia');
    Route::get('/adreport/byadvertiser', 'advertiser\adreportController@ByAdvertiser')->name('ad.byadvertiser');
    Route::get('/adreport/byperiod', 'advertiser\adreportController@ByPeriod')->name('ad.byperiod');
    // log
    Route::resource('adlog', 'advertiser\adlogController');
});
