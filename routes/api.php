<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/inbound/inbound', 'Api\InboundController@list');

Route::get('/inbound/ip', 'Api\InboundController@ip');
Route::get('/inbound/entry', 'Api\InboundController@entry');
Route::get('/inbound/apppackage', 'Api\InboundController@apppackage');
Route::get('/inbound/appinstalled', 'Api\InboundController@appinstalled');

Route::get('/preferences/setting', 'Api\preferencesController@setting');
Route::get('/lockscreen/contents', 'Api\lockscreenController@contents');
Route::post('/point/callback', 'Api\pointController@callback');
Route::get('/log', 'Api\logController@log');

Route::get('/taproad/callback','Api\taproadController@callback');
Route::post('/taproad/callback','Api\taproadController@callback');

Route::get('/tapjoy/callback','Api\tapjoyController@callback');

Route::post('/appall/callback','Api\appallController@callback');

Route::get('/point/history','Api\pointController@userPointHistory');
